import { defineConfig } from 'umi'
//用来定义配置的
//.umirc.ts它的优先级 要比config要高
import routes from './routes'


export default defineConfig({
    title:"创作平台后台管理",
    routes,
    layout:{
        name:'八维创作平台',
        logo:'',
        siderWidth:200
    },
    proxy:{
        '/api':{
            target:'https://creationapi.shbwyz.com',  // 接口域名
            changeOrigin:true,
            pathRewrite:{
                "^/api":""
            }
        },
        '/abi':{
            target:'http://localhost:9000',
            changeOrigin:true,
            pathRewrite:{
                "^/abi":""
            }
        }

    },
    dva:{
        immer:true,
        hmr:true
    }
})