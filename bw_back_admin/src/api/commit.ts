import {request} from 'umi';


export const reply = (params = {
    content: '',
    createByAdmin: true,
    email: '',
    hostId: '',
    name: '',
    parentCommentId: '',
    replyUserEmail: '',
    replyUserName: '',
    url: ''
  }) => request('/api/api/comment', {
    method: 'POST',
    skipErrorHandler: true,
    data: params
  })

export const getcomment = (params: any = { page: 1, pageSize: 12, name: " ", email: "", pass: "" }) =>
  request('/api/api/comment', {
    method: 'get',
    skipErrorHandler: true,
    params
  })