import {request} from 'umi'
import {BasePageParams} from "../api/api.d"

export interface paramsType{
    page:number,
    pageSize:number
}

export const get_set=()=>request('https://creationapi.shbwyz.com/api/setting/get',{
    method:'POST'
})

export const get_article=(params:paramsType={page:1,pageSize:12})=>request('https://creationapi.shbwyz.com/api/article',{
    params,
    method:'get',
})
// echarts图
export const getChartData=()=>new Promise((resolve)=>{
    setTimeout(()=>{
        resolve({
            success:true,
            msg:"获取数据成功",
            data:[
                new Array(7).fill("").map(()=>Math.floor(Math.random()*100)+1),
                new Array(7).fill("").map(()=>Math.floor(Math.random()*100)+1),

            ]
        })
    },500)
})
//工作台 获取文章列表
export const _getArticalList=(params:BasePageParams={page:1,pageSize:6})=>request(`/api/api/article`,{params})

//知识小测
export const get_knowledge=(params:BasePageParams={page:1,pageSize:6})=>request(`/api/api/knowledge`,{params})

//页面管理
export const get_pagemanager=(params:BasePageParams={page:1,pageSize:10})=>request(`/api/api/page`,{params})

// 下线//发布
export const edit_pagemanager=(params:any)=>request(`/api/api/page/${params.id}`,{
    method:'patch',
    data:params
})
// 删除页面数据
export const del_pagemanager=(params:string)=>request(`/api/api/page/${params}`,{
    method:'delete',
})
//最新评论
export const get_newcomment=()=>request('/api/api/comment',{
    method:'get',
})

export const get_tag=()=>request('/api/api/category',{
    method:'get',
})
export const get_label=()=>request('/api/api/tag',{
    method:'get',
})
export const get_user=(params:BasePageParams={page:1,pageSize:10})=>request('/api/api/user',{
    method:'get',
    params
})




export const get_commont=(params:{page:1,pageSize:10})=>request('/api/api/comment',{
    // method:'get',
    params
})
// 添加标签
export const add_tag=(params:any)=>request('/api/api/tag',{
    method:'post',
    data:params,
})
// 删除标签
export const del_tag=(params:any)=>request(`/api/api/tag/${params}`,{
    method:'delete',
    data:params
})
// 编辑标签
export const edit_tag=(params:any)=>request(`/api/api/tag/${params.id}`,{
    method:'patch',
    data:params,
    skipErrorHandler:true
})
// 添加分类
export const add_category=(params:any)=>request('/api/api/category',{
    method:'post',
    data:params,
})
// 删除分类
export const del_category=(params:any)=>request(`/api/api/category/${params}`,{
    method:'delete',
    data:params
})
// 编辑分类
export const edit_category=(params:any)=>request(`/api/api/category/${params.id}`,{
    method:'patch',
    data:params,
    skipErrorHandler:true
})
// 首焦变撤销推荐
export const edit_article=(params:any)=>request(`/api/api/article/${params.id}`,{
    method:'patch',
    data:params
})
//删除
export const del_article=(params:string)=>request(`/api/api/article/${params}`,{
    method:'delete',
    data:params
})