import { request } from 'umi'
//知识小册
export const del_knowledge=(id:string)=>request(`/api/api/knowledge/${id}`,{
    method:'DELETE'
})
export const edit_knowStatus=(params:any)=>request(`/api/api/knowledge/${params.id}`,{
    method:'patch',
    data:params
})
export const get_knowList=(params:{page:number,pageSize:number})=>request('/api/api/file',{
    method:'get',
    params
})
export const edit_knowList=(params:any)=>request(`/api/api/knowledge/${params.id}`,{
    method:'patch',
    data:params
})
export const add_knowList=(params:any)=>request(`/apiapi/Knowledge/book`,{
    method:'post',
    data:params
})