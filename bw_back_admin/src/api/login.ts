import { request } from 'umi'

export interface LoginData{
    name:string,
    password:string
}

export const _login=(data:LoginData)=>request('https://creationapi.shbwyz.com/api/auth/login',{
    method:'POST',
    data,
    isAuthorization:'false',//不需要携带身份标识
})