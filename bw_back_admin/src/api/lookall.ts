import { request } from 'umi'

export interface paramsType{
    page:number,
    pageSize:number
}

export const get_look_all=(params:paramsType={page:1,pageSize:12})=>request('https://creationapi.shbwyz.com/api/view',{
    params,
    method:'get'
})


export const delete_look=(id:string)=>request(`https://creationapi.shbwyz.com/api/view/${id}`,{
    method:'DELETE'
})