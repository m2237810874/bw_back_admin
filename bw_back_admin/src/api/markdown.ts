import { request } from "umi";

export const addArticle = (params: any) =>
  request('/api/api/article', {
    method: 'post',
    skipErrorHandler: true,
    data: params
  });
export const getcategory = () =>
  request('/api/api/category', {
    method: 'get',
    skipErrorHandler: true,
  });
export const gettag = () =>
  request('/api/api/tag', {
    method: 'get',
    skipErrorHandler: true,
  });
export const getfile = (params: any = { page: 1, pageSize: 12, originalname: "", type: "" }) =>
  request(`/api/api/file`, {
    method: 'get',
    skipErrorHandler: true,
    params
  })
export const getknow = (params: any = { page: 1, pageSize: 12, title: '', status: '' }) =>
  request('/api/api/knowledge', {
    methods: 'get',
    skipErrorHandler: true,
    params
  })
export const editArticle = (params: any) =>
  request(`/api/api/article/${params.id}`, {
    method: 'patch',
    data: params,
    skipErrorHandler: true,
  });
export const deleteItem = (id: string) =>
  request(`/api/api/article/${id}`, {
    method: 'delete',
    skipErrorHandler: true,
  });