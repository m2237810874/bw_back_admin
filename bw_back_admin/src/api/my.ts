import { request } from 'umi';
// import { tableParams, loginData, userData, updatePswData } from '@/types';
//*修改用户信息
export const updateUser = (data: any) =>
  request('/api/api/user/update', {
    method: 'POST',
    data: data,
    isAuthorization: false,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  });
//*更改密码
export const updatePsw = (data: any) =>
  request('/api/api/user/password', {
    method: 'POST',
    data: data,
    isAuthorization: false,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  });
/* 
      -- 个人中心 --
*/
//*分类管理
export const getCategory = () =>
  request('/api/api/category', {
    method: 'get'
  });
  //*获取文章管理列表
export const getArticle = (params: any) =>
request('/api/api/article', {
  method: 'get',
  params
});
//*获取评论列表
export const getComment = (params: any) =>
  request('/api/api/comment', {
    method: 'get',
    params
  });
//*获取文件管理列表
export const getFile = (params: any) =>
  request('/api/api/file', {
    method: 'get',
    params
  });
  //*标题管理
export const getTags = () =>
request('/api/api/tag', {
  method: 'get'
});