import { request } from "umi";

export const addpage = (value:any) =>
  request(`/api/api/page`, {
    method: 'post',
    skipErrorHandler: true,
    data:value
  });

  export const editpage = (params: any) =>
  request(`/api/api/page/${params.id}`, {
    method: 'patch',
    data: params,
    skipErrorHandler: true,
  });
  export const deletepage = (id: string) =>
  request(`/api/api/page/${id}`, {
    method: 'delete',
    skipErrorHandler: true,
  });
  export const _upload = (params:any)=>request('/api/api/file/upload',{
    method:'post',
    data:params
})