import { request } from 'umi'


export interface paramsType{
    page:number,
    pageSize:number
}


export const get_searchlist=(params:paramsType={page:1,pageSize:12})=>request('https://creationapi.shbwyz.com/api/search',{
    params,
    method:'get'
})


export const del_searchList=(id:string)=>request(`/api/api/search/${id}`,{
    method:'DELETE'
})