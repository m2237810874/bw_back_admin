import { request } from 'umi'

export const get_system=()=>request('/api/api/setting/get',{
    method:'post',
    data:JSON.parse(localStorage.getItem('user') as any)
})