import { request } from 'umi'


export const del_work_commont=(id:string)=>request(`/api/api/comment/${id}`,{
    method:'DELETE'
})


export const commit_adopt=(id:string)=>request(`/api/api/comment/${id}`,{
    method:'PATCH',
    data:{
        pass:true
    }
})


export const commit_no=(id:string)=>request(`/api/api/comment/${id}`,{
    method:'PATCH',
    data:{
        pass:false
    }
})
