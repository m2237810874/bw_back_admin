
// request 统一配置
// 统一处理请求公共参数  携带token身份表示  csrf-token
// 错误的统一处理
import {RequestConfig,history} from 'umi'
// 启用initial-state 插件
import { createLogger } from 'redux-logger';
import { Button , Dropdown , Menu , message } from 'antd';

interface Headers {
    authorization?:string | undefined | null,
    [propName:string]:any
}


export const getInitialState=()=>{//该函数  在页面初始化时 执行一次  而且只执行一次
    // 容错处理
    try{
        const uesrInfo=localStorage.getItem('token')//获取本地存储的用户信息
        if(!uesrInfo){
            history.replace('/login')
        }
        return uesrInfo
    }catch(error){
        console.log('error');
    }
}



const TOKEN_TYPE='Bearer ';

export const request:RequestConfig={
    timeout:20000,
    errorConfig:{
        adaptor:(resDate)=>{
            return{
                ...resDate,
                errorMessage:resDate.msg //  success为false的时候  弹框提示errorMessage信息
            }
        }
    },
    requestInterceptors:[ //请求拦截器
        (url,options)=>{
            console.log('接口请求前执行，处理请求接口时携带的公共参数',options);
            const headers:any={
                ...options.headers
            }
            // 判断权限有没有
           
            if(!options.isAuthorization){  // 获取本地存储里面的token
                headers['authorization']=`${TOKEN_TYPE} ${localStorage.getItem('token')}`
            }
            return {
                url:`${url}`,//请求地址
                options:{
                    ...options,
                    interceptors:true,
                    headers
                }
            }
        }
    ],
    responseInterceptors:[
        async response=>{
            const res=await response.json()
            if(res.statusCode===401){
                res.msg=`您暂时没有该接口权限`
            }
            if(res.statusCode===401){
                history.replace('/login')
            }
            return res
        }
    ]
}

export const dva = {
    config: {
      onAction: createLogger(),
      onError(e: Error) {
        message.error(e.message, 3);
      },
    },
  };


  export const layout = () => {
    const menu = (
        <Menu
            onClick={({key , domEvent}) => {
                //  获取原生事件
                domEvent.stopPropagation();
                history.push(key);
            }}
            items={
                [
                    {
                        key:'/article/markdown',
                        label:'新建文章',
                    },
                    {
                        key:'/create/page',
                        label:'新建页面'
                    }
                ]
        }>
        </Menu>
    );

    const mymenu=(
        <Menu
                items={[
                {
                    key: '1',
                    label: <a href="/myCenter">个人中心</a>,
                },
                {
                    key: '2',
                    label: <a href="/users">用户管理</a>,
                },
                {
                    key: '3',
                    label: <a href="/setsystem">系统设置</a>,
                },
                {
                    key: '4',
                    label: (
                    <span
                        onClick={() => {
                        localStorage.clear();
                        history.push('/login');
                        localStorage.clear()
                        }}
                    >
                        退出登录
                    </span>
                    ),
                },
                ]}
            />
    )

    const user=JSON.parse(window.localStorage.getItem('user') as any) 
    const data=JSON.parse(window.localStorage.getItem('syemeater') as any)
    return {
        rightContentRender:() => <div>
            <Dropdown overlay={mymenu} placement="bottom">
                <p style={{display:'flex',alignItems:'center'}}>
                <a style={{display:'inline-block',color:'#000',fontSize:'28px'}} href="https://github.com/fantasticit/wipi" target="_blank" rel="noreferrer"><span role="img" aria-label="github" class="anticon anticon-github"><svg viewBox="64 64 896 896" focusable="false" data-icon="github" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M511.6 76.3C264.3 76.2 64 276.4 64 523.5 64 718.9 189.3 885 363.8 946c23.5 5.9 19.9-10.8 19.9-22.2v-77.5c-135.7 15.9-141.2-73.9-150.3-88.9C215 726 171.5 718 184.5 703c30.9-15.9 62.4 4 98.9 57.9 26.4 39.1 77.9 32.5 104 26 5.7-23.5 17.9-44.5 34.7-60.8-140.6-25.2-199.2-111-199.2-213 0-49.5 16.3-95 48.3-131.7-20.4-60.5 1.9-112.3 4.9-120 58.1-5.2 118.5 41.6 123.2 45.3 33-8.9 70.7-13.6 112.9-13.6 42.4 0 80.2 4.9 113.5 13.9 11.3-8.6 67.3-48.8 121.3-43.9 2.9 7.7 24.7 58.3 5.5 118 32.4 36.8 48.9 82.7 48.9 132.3 0 102.2-59 188.1-200 212.9a127.5 127.5 0 0138.1 91v112.5c.8 9 0 17.9 15 17.9 177.1-59.7 304.6-227 304.6-424.1 0-247.2-200.4-447.3-447.5-447.3z"></path></svg></span></a>
                <img style={{width:'30px',height:'30px',borderRadius:'50%',marginLeft:'10px'}} src={user.avatar} alt="" />
                <span style={{fontSize:'20px'}}>Hi,{user.name}</span>
                </p>
            </Dropdown>
        </div>,
        footerRender: () => {<div>footer1334</div>},
        menuHeaderRender : (logo:any , title:string) => {
            return <div>
                <img src={data.data.systemFavicon} alt="" />
                {title}
                <Dropdown overlay={menu} placement="bottom">
                    <Button type="primary" size="large" style={{marginTop:'10px',width:'100%'}}>＋新建</Button>
                </Dropdown>
            </div>
        }
    }
};