import { FC, useState } from 'react'
import { Button, Drawer , message, Popconfirm } from 'antd';
import style from './brawer.less'



const Brawer: FC = ({ onClose, Open , imgData}: any) => {
    const confirm = (e: React.MouseEvent<HTMLElement>) => {
        console.log(e);
        message.success('Click on Yes');
    };
    const cancel = (e: React.MouseEvent<HTMLElement>) => {
        console.log(e);
        message.error('Click on No');
      };
    return (
        <Drawer 
            title="文件信息"
            placement="right" 
            onClose={onClose} 
            open={Open} 
            width='40%'
            footer={
                <div className={style.brawerFooter}>
                    <Button style={{marginRight:'5px'}} onClick={onClose}>关闭</Button>
                    <Popconfirm
                        title="确认删除吗"
                        onConfirm={confirm}
                        onCancel={cancel}
                        okText="是"
                        cancelText="否"
                    >
                        <Button danger>删除</Button>
                    </Popconfirm>
                </div>
            }
            
            
            >
            <div className={style.mainimgDiv}>
                <img src={imgData.url} alt="" />
            </div>
            <div style={{padding:'20px'}}>
                <p>
                    文件名称：{imgData.originalname}
                </p>
                <p>
                    存储路径： {imgData.filename}
                </p>
                <p className={style.two}>
                    <span>
                        文件类型：{imgData.type}
                    </span>
                    <span>
                        文件大小：{(imgData.size/1024).toFixed(2)}KB
                    </span>
                </p>
                <div className={style.textbox}>
                    <div>
                        访问链接：
                    </div>
                    <div className={style.text}>
                        <textarea className={style.text} style={{ width: '100%', border: "1px solid #ccc" }}>
                            {imgData.url}
                        </textarea>
                        <a>复制</a>
                    </div>
                </div>
            </div>
        </Drawer>
    )
}

export default Brawer