import { FC, useState } from 'react'
import style from './callback.less'
import { message, Modal } from 'antd';
import { reply,getcomment } from '@/api/commit'

const CallBack: FC = ({ isModalOpen, setIsModalOpen, All }: any) => {
    let name=JSON.parse(localStorage.getItem('userInfo') as any)
    
    const handleCancel = () => {
      setIsModalOpen(false);
    };
    const handleOk = async () => {
      if (value) {
        setIsModalOpen(false);
        setValue('')
      }
      let res = await reply({
        content: value,
        createByAdmin: true,
        email: All.email,
        hostId: All.hostId,
        name: name.name,
        parentCommentId: All.id,
        replyUserEmail: All.replyUserEmail,
        replyUserName:All.name,
        url:All.url
      })
      if ( res.statusCode === 201) {
       
          await getcomment()
        
        // getcomment({page:1,pageSize:12})
        message.success('回复成功')
      }else{
        message.success('回复失败')
      }
      
    };
    const [value, setValue] = useState<any>('')
    return (
      <Modal title="回复评论" open={isModalOpen} onOk={handleOk} onCancel={handleCancel}>
        <textarea className={style.inp} placeholder={'支持Mackdown'}
          value={value}
          onChange={(e) => {
            setValue(e.target.value);
          }}
        ></textarea>
      </Modal>
    )
  }
  
  export default CallBack;