 import { EChartsType,init,EChartsOption,dispose } from "echarts"
import {useRef,useEffect} from "react"

type Types="pie"|"bar"|"line" //饼状图，折线图，柱状图

interface OptionsType{
    type:Types,
    series:any[],
    xData?:any[],
    yData?:any[],
    chartData?:any
}

// 定义宽高
interface MyProps extends OptionsType{
    width?:number,
    height?:number,
    loading?:boolean

}

//格式化数据
const formatOption=({
  type,
  series,
  xData,
  yData,
  chartData

}:OptionsType):EChartsOption=>{
    switch(type){
        case "pie":
            return {
                ...chartData,
                series,
                
            }
        case "bar":return {
                ...chartData,
                xAxis:{
                    type:"category",
                    data:xData,
                    
                },
                yAxis:{
                    type:"value"
                },
                series
            }
        case "line":
            return {
                ...chartData,
                xAxis:{
                    type:"category",
                    data:xData
                },
                yAxis:{
                    type:"value"
                },
                series
            }
        }
    }
    
    const BaseChart=({
        type="bar",
        series=[], //图标主数据
        xData=[], //x轴数据
        yData=[], //y轴数据
        loading=true,
        chartData={},
        width=1300,
        height=400,
    }:MyProps)=>{
        //获取实例
    const chartInstance=useRef<null | EChartsType>(null);
    const chartDom=useRef<null | HTMLDivElement>(null)
    useEffect(()=>{
        if(chartDom.current){
            chartInstance.current=init(chartDom.current,"",{
                width,
                height
            })
        }
        return()=>{
            chartInstance.current?.dispose(); //图表和图表内组件的卸载
            chartInstance.current && dispose(chartInstance.current); //卸载dom所有的方法

        }
    },[]);
    useEffect(()=>{
        if(chartInstance.current){
            chartInstance.current.setOption(formatOption({
                type,
                series,
                xData,
                yData,
                chartData
            }))
        }
        
    },[type,series,xData,yData,chartInstance])
    useEffect(()=>{
        if(chartInstance.current){
            loading ? chartInstance.current.showLoading():chartInstance.current.hideLoading()
        }
    },[loading,chartInstance])
    return (
        <div ref={chartDom}>
           echarts
        </div>
    ) 
}


export default BaseChart