// import React from 'react'
import { Select } from 'antd'
interface OptionItemType{
    title:string,
    key:string,
}
interface Props {
    options:OptionItemType[],
    title?:string,
    onChange?:()=>{}
}

const BaseSelects:any = ({options,title="",onChange}:Props) => {

    const mychange=(value)=>{
        onChange()
        console.log(value);
        
    }
  return (
    <Select placeholder={'请选择'+title}>
        {
            options.map((item:OptionItemType)=>{
                return <Select.Option key={item.key} onChange={()=>mychange()} value={item.key}>
                    {item.title}
                </Select.Option>
            })
        }
    </Select>
  )
}

export default BaseSelects