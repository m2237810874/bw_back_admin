import React, { useState, useEffect } from 'react';
import { Drawer, Button, Form, Input, Switch, Select } from 'antd';
const { TextArea } = Input;
import Drawers from '../../components/drawers';
import styles from './drawer.less';
import { getcategory, gettag } from '@/api/markdown';
interface MyProps {
  open: boolean;
  onClose(): void;
  newCreate(val: any): void;
  elist?: any;
}

export default function index(props: any) {
  // 选择文件抽屉
  const { open, onClose, newCreate, elist } = props;
  console.log(elist);
  
  const [clist, setclist] = useState([]);
  const [type, settype] = useState([]);
  const [myForm] = Form.useForm();
  const getCategory = async () => {
    let res = await getcategory();
    settype(res.data || []);
  };
  const getTags = async () => {
    let res = await gettag();
    setclist(res.data || []);
  };

  useEffect(() => {
    getCategory();
    getTags();
    myForm.resetFields();
    myForm.setFieldsValue(elist);
  }, []);
  const onFinish = (values: any) => {
    console.log('Success:', values);
    newCreate({
      summary: values.summary,
      password: values.password,
      totalAmount: values.totalAmount,
      isCommentable: values.isCommentable,
      isRecommended: values.isRecommended,
      category: values.category,
      tags: values.tags,
      cover: Imgurl,
    });
    onClose();
  };
  // 选择抽屉
  const [Imgurl, setImgurl] = useState('');
  const SETimgurl = (url: string) => {
    setImgurl(url);
  };
  const [opens, setOpens] = useState(false);
  const showDrawer = () => {
    setOpens(true);
  };

  const onCloses = () => {
    setOpens(false);
  };
  return (
    <Drawer
      title="文章设置"
      placement="right"
      onClose={onClose}
      open={open}
      width={'500px'}
    >
      <Form
        name="basic"
        wrapperCol={{ span: 16 }}
        initialValues={{ remember: true }}
        onFinish={onFinish}
        autoComplete="off"
        form={myForm}
        style={{padding:'24px'}}
      >
        <Form.Item label="文章摘要" name="summary">
          <TextArea rows={4} />
        </Form.Item>

        <Form.Item label="访问密码" name="password">
          <Input.Password />
        </Form.Item>
        <Form.Item label="付费查看" name="totalAmount">
          <Input.Password />
        </Form.Item>
        <Form.Item
          label="开启评论"
          valuePropName="checked"
          name="isCommentable"
        >
          <Switch />
        </Form.Item>
        <Form.Item
          label="首页推荐"
          valuePropName="checked"
          name="isRecommended"
        >
          <Switch />
        </Form.Item>
        <Form.Item label="选择分类" name="category">
          <Select>
            {type.map((item: any, i: number) => {
              return (
                <Select.Option key={i} value={item.id}>
                  {item.label}
                </Select.Option>
              );
            })}
          </Select>
        </Form.Item>
        <Form.Item label="选择标签" name="tags">
          <Select>
            {clist.map((item: any, i: number) => {
              return (
                <Select.Option key={i} value={item.id}>
                  {item.label}
                </Select.Option>
              );
            })}
          </Select>
        </Form.Item>
        <Form.Item label="封面" name="cover">
          <div onClick={showDrawer} className={styles.knowdragg}>
            {Imgurl ? (
              <img src={Imgurl} className={styles.knowimg} alt="" />
            ) : (
              <p>预览图，请点击设置封面</p>
            )}
          </div>
          <div className={styles.covers}>
            <Input value={Imgurl} placeholder="或输入外部链接" />
          </div>
          <div className={styles.DrawerFooter}>
            <Button onClick={showDrawer}>选择文件</Button>
            <Button danger>移除</Button>
          </div>
          <Drawers
            opens={opens}
            onCloses={onCloses}
            SETimgurl={SETimgurl}
          ></Drawers>
        </Form.Item>
        <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
          <Button type="primary" htmlType="submit">
            确认
          </Button>
        </Form.Item>
      </Form>
    </Drawer>
  );
}
