import React, { useEffect, useState } from 'react';
import { PlusOutlined, InboxOutlined } from '@ant-design/icons';
import {Drawer,Pagination} from 'antd';
import styles from './drawers.less';
import type { UploadProps } from 'antd';
import {
  ProFormSelect,
  ProFormText,
  QueryFilter,
} from '@ant-design/pro-components';
import { getfile, getknow } from '@/api/markdown';
import { BasePageParams } from '@/api/api';
import type { PaginationProps } from 'antd';

interface MyProps {
  opens: boolean;
  onCloses(): void;
  SETimgurl(url: string): void;
}

function index(props: MyProps) {
  // 选择文件抽屉
  const { opens, onCloses, SETimgurl } = props;
  const [fileList, setFileList] = useState<any[]>([]);
  const [total, setTotal] = useState(0);
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(12);
  const getlist = async ({
    page,
    pageSize,
    originalname,
    type,
  }: BasePageParams) => {
    let res = await getfile({ page, pageSize, originalname, type });
    setFileList(res.data[0]);
    setTotal(res.data[1]);
  };
  useEffect(() => {
    getlist({ page: 1, pageSize: 12, originalname, type });
  }, []);
  // 分页展示数据总条数
  const showTotal: PaginationProps['showTotal'] = (total: number) =>
    `共 ${total} 条`;

  // 分页改一页几条
  const onChange: PaginationProps['onChange'] = (page, pageSize) => {
    setPageSize(pageSize);
    setPage(page);
    getlist({ page, pageSize, originalname, type });
  };
  //搜索
  const [originalname, setoriginalname] = useState('');
  const [type, settype] = useState('');

  const [liste, setLists] = useState([]);

  const getList = async ({ page, pageSize, title, status }: BasePageParams) => {
    let res = await getknow({ page, pageSize, title, status });
    setLists(res.data[0] || []);
  };

  useEffect(() => {
    getList({ page: 1, pageSize: 12 });
  }, []);

  return (
    <Drawer
      title="文件选择"
      placement="right"
      onClose={onCloses}
      open={opens}
      width={800}
    >
      <div className={styles.fileDrawer}>
        <div className={styles.fileDrawerHeader}>
          <QueryFilter
            defaultCollapsed={false}
            defaultColsNumber={5}
            onFinish={async (values) => {
              setoriginalname(values.originalname);
              settype(values.type);
              await getlist({
                page,
                pageSize,
                originalname: values.originalname,
                type: values.type,
              });
            }}
          >
            <ProFormText name="originalname" label="文件名" />
            <ProFormText name="type" label="文件类" />
          </QueryFilter>
        </div>
        <div className={styles.fileDrawerBody}>
          {fileList.map((item, index) => {
            return (
              <div
                key={index}
                className={styles.fileDrawerBodyItem}
                onClick={() => {
                  SETimgurl(item.url);
                  onCloses();
                }}
              >
                <div className={styles.fileDrawerBodyItem_img}>
                  <img src={item.url} alt="" />
                </div>
                <div className={styles.fileDrawerBodyItem_name}>
                  <span>{item.originalname}</span>
                </div>
              </div>
            );
          })}
          <div className={styles.fileDrawerBodyPage}>
            <Pagination
              total={total}
              showTotal={showTotal}
              pageSize={pageSize}
              onChange={onChange}
              current={page}
            />
          </div>
        </div>
      </div>
    </Drawer>
  );
}

export default index;
