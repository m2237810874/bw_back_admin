import React,{Component} from "react";
import  Child from '../child'


class Parent extends Component{
    render(){
        return(
            <div>
                <h3>
                    我是父组件
                    <Child/>
                </h3>
            </div>
        )
    }
}

export default Parent