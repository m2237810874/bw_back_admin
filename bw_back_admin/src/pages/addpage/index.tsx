import { useEffect, useRef, useState } from 'react';
import style from './addpage.less';
import MDEditor, { commands } from '@uiw/react-md-editor';
import content from '@/config/defaultArticleCont';
import _, { values } from 'lodash';
import { useDispatch, useSelector } from 'dva'
import { Upload, Button, message, Popconfirm, Menu, Dropdown, Form, Input, Drawer, Tooltip, InputNumber, Card, Pagination } from 'antd';
import {
    UploadOutlined,
    CloseSquareOutlined,
    EllipsisOutlined,
    CopyOutlined
} from '@ant-design/icons';
import { history } from 'umi';
import { addpage, _upload } from '@/api/page';
interface Menun {
    title: string;
    type: string;
    level: number;
    key: number;
}
const gridStyle: React.CSSProperties = {
    width: '23%',
    padding: '0',
    textAlign: 'center',
};
export default function index() {
    const [value, setValue] = useState(() => content);
    const [menuData, setmenuData] = useState<Menun[]>([]);
    const preview = useRef<any>();
    const [title, settitle] = useState('');
    const [open, setOpen] = useState(false);
    const dispatch = useDispatch()
    const [form] = Form.useForm()
    useEffect(() => {
        dispatch({
            type: 'knowledgebooks/knowList',
            payload: {
                page: 1,
                pageSize: 12
            }
        })
    }, [dispatch])
    const { knowList } = useSelector(({ knowledgebooks }: { knowledgebooks: any }) => {
        return {
            ...knowledgebooks
        }
    })
    const pageChange = (page: number, pageSize: number) => {
        dispatch({
            type: 'knowledgebooks/knowList',
            payload: {
                page,
                pageSize
            }
        })
    }
    const showDrawer = () => {
        setOpen(true);
    };
    const onClose = () => {
        setOpen(false);
    };
    const handleChange = (e: any) => {
        // 写文字
        setValue(e);
    };
    const handleUpload = async (file: any) => {
        //覆盖默认上传行为，实现自定义上传
        console.log(file);
        const formData = new FormData();
        formData.append('file', file.file);
        const res = await _upload(formData);
        message.success('图片上传成功');
    };
    const formatMenuData = () => {
        const previewEl = preview.current.querySelector('.w-md-editor-preview');
        const menuDataEl = Array.from(
            previewEl.querySelectorAll('*'),
        ).filter((item: any) => /^H[1-6]$/.test(item.nodeName));
        setmenuData(
            menuDataEl.map((item: any, i: number) => ({
                title: item.innerText,
                type: item.nodeName,
                level: item.nodeName.slice(1) * 1,
                key: i,
            })),
        );
    };
    const onChange = (value: number) => {
        console.log('changed', value);
    };
    const fileprops: any = {
        name: 'file',
        action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
        headers: {
            authorization: 'authorization-text',
        },
        onChange(info) {
            if (info.file.status !== 'uploading') {
                console.log(info.file, info.fileList);
            }
            if (info.file.status === 'done') {
                message.success(`${info.file.name} file uploaded successfully`);
            } else if (info.file.status === 'error') {
                message.error(`${info.file.name} file upload failed.`);
            }
        },
    };
    const [pageopen, setpageOpen] = useState(false);
    const [imglis, setImglis] = useState('')
    const [Value, setvalue] = useState([])
    const pageonClose = () => {
        setpageOpen(false);
    };
    const menu = (
        <Menu
            items={[
                {
                    key: '1',
                    label: <a>查看</a>,
                    disabled: true,
                },
                {
                    key: '2',
                    label: (
                        <a
                            onClick={() => {
                                if (title) {
                                    showDrawer();
                                } else {
                                    message.warning('请输入文章标题');
                                }
                            }}
                        >
                            设置
                        </a>
                    ),
                },
                {
                    key: '3',
                    label: <a>保存草稿</a>,
                },
                {
                    key: '4',
                    label: <a>删除</a>,
                    disabled: true,
                },
            ]}
        />
    );

    useEffect(() => {
        setTimeout(() => {
            if (preview.current && value) {
                formatMenuData();
            }
        }, 0);
    }, [preview, value]);
    console.log(form);

    return (
        <div className={style.addpage}>
            <header>
                <div className={style.addpage_header}>
                    <div className={style.addpage_header_left}>
                        <div className={style.addpage_header_box}>
                            <div className={style.addpage_header_box_left}>
                                <Button>
                                    <Popconfirm
                                        title="确认关闭？如果有内容变更，请先保存"
                                        onConfirm={() => history.goBack()}
                                        okText="确认"
                                        cancelText="取消"
                                    >
                                        <CloseSquareOutlined width='25' height='25px' />
                                    </Popconfirm>
                                </Button>
                            </div>
                            <div className={style.addpage_header_box_right}>
                                <div className={style.addpage_header_box_right_input}>
                                    <input placeholder='请输入文章标题' style={{
                                        border: '0',
                                        borderBottom: '1px solid #d9d9d9',
                                        width: '300px',
                                    }}
                                    value={title}
                                        onChange={(e) => {
                                            settitle(e.target.value);
                                        }}>
                                    </input>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className={style.addpage_header_right}>
                        <div className={style.addpage_header_right_botton}>
                            <Button type="primary" onClick={() => {
                                addpage({
                                    title:title,
                                    ...Value
                                })
                                message.success('添加页面成功')
                            }}>
                                发布
                            </Button>
                        </div>
                        <div className={style.addpage_header_right_set}>
                            <Dropdown overlay={menu}>
                                <EllipsisOutlined />
                            </Dropdown>
                        </div>
                    </div>
                </div>
            </header>
            <MDEditor
                className={style.addpage}
                value={value}
                onChange={_.throttle(handleChange, 500)}
                ref={(val: any) => {
                    if (val) {
                        preview.current = val.container;
                    }
                }}
                commands={[
                    //自定义toolbar
                    {
                        name: 'test',
                        keyCommand: 'test',
                        icon: (
                            <Upload
                                accept="image/png,image/jpeg,image/gif,image/jpg,image/svg"
                                showUploadList={false}
                                customRequest={handleUpload}
                            >
                                <Button type="link" icon={<UploadOutlined />}></Button>
                            </Upload>
                        ),
                    },
                    commands.divider,
                    commands.bold,
                    commands.checkedListCommand,
                    commands.code,
                    commands.fullscreen,
                    commands.italic,
                    commands.image,
                    commands.strikethrough,
                    commands.quote,
                ]}
            >
                <MDEditor.Markdown source={value} className={style.con} />
            </MDEditor>
            <div className={style.menu}>
                <h3>大纲</h3>
                {menuData &&
                    menuData.map((item) => {
                        return (
                            <p key={item.key} style={{ marginLeft: item.level * 6 }}>
                                {item.title}
                            </p>
                        );
                    })}
            </div>
            <Drawer title="页面属性" placement="right" onClose={onClose} open={open}>
                <Form form={form} style={{ padding: '24px', width: '100%' }} onFinish={(values) => {
                    console.log(values);
                    setvalue(values)
                    setOpen(false)
                }}>
                    <Form.Item name='cover' style={{
                        display: 'flex',
                        alignItems: 'center',
                        width: '100%'
                    }}>
                        <span style={{
                            marginRight: '15px'
                        }}>封面</span>
                        <Input
                            value={imglis}
                            style={{ width: 'calc(100% - 100px)' }}
                            placeholder='请输入页面封装'
                        />
                        <Tooltip title="copy git url">
                            <Button onClick={() => {
                                setpageOpen(true)
                            }} icon={<CopyOutlined />} />
                        </Tooltip>
                    </Form.Item>
                    <div style={{ display: 'flex' }}>
                        <span style={{
                            marginRight: '15px',
                            width: '35px'
                        }}>路径</span>
                        <Form.Item name='path' style={{
                            display: 'flex',
                            alignItems: 'center',
                            width: '100%'
                        }}>

                            <Input />
                        </Form.Item>
                    </div>
                    <Form.Item>
                        <span style={{
                            marginRight: '15px'
                        }}>顺序</span>
                        <Form.Item style={{
                            display: 'inline-block'
                        }} name='order'>
                            <InputNumber size="small" min={1} max={100000} defaultValue={3} onChange={onChange} />
                        </Form.Item>
                    </Form.Item>
                    <Form.Item>
                        <Button type="primary" htmlType='submit' style={{
                            position: 'absolute',
                            bottom: '20px',
                            right: '20px'
                        }}>确认</Button>
                    </Form.Item>
                </Form>
            </Drawer>
            <Drawer width='780px' title="文件选择" placement="right" onClose={pageonClose} open={pageopen}>
                <div className={style.file}>
                    <Form>
                        <div className={style.file_top}>
                            <div className="file_name">
                                <span>文件名</span>
                                <Form.Item style={{ display: 'inline-block', marginBottom: '0' }}>
                                    <Input placeholder='请输入文件名称'></Input>
                                </Form.Item>
                            </div>
                            <div className="file_class">
                                <span>文件类</span>
                                <Form.Item style={{ display: 'inline-block', marginBottom: '0' }}>
                                    <Input placeholder='请输入文件类型'></Input>
                                </Form.Item>
                            </div>
                        </div>
                        <div className={style.file_bottom}>
                            <Button type="primary" style={{ marginRight: '15px' }}>搜索</Button>
                            <Button>重置</Button>
                        </div>
                    </Form>
                    <div className={style.file_list}>
                        <div className={style.file_list_top}>
                            <Upload {...fileprops}>
                                <Button>上传文件</Button>
                            </Upload>
                        </div>
                        <div className='file_list_bottom' style={{ marginTop: '20px' }}>
                            <Card>
                                {
                                    knowList[0] && knowList[0].map((item: any, index: number) => {
                                        return <Card.Grid key={index} style={gridStyle} onClick={() => {
                                            setImglis(item.url)
                                            form.resetFields();
                                            form.setFieldsValue({
                                                cover: item.url
                                            })
                                            setpageOpen(false)
                                            console.log(item.url);

                                        }}>
                                            <img width='100%' height='100px' src={item.url} alt="" style={{ objectFit: 'cover' }} />
                                            <p style={{
                                                whiteSpace: 'nowrap',
                                                textOverflow: 'ellipsis',
                                                overflow: 'hidden',
                                                display: 'block',
                                                padding: '20px 10px 20px 10px'
                                            }}>{item.originalname}</p>
                                        </Card.Grid>
                                    })
                                }
                            </Card>
                        </div>
                        <div className={style.pagesition}>
                            <Pagination
                                total={knowList[1]}
                                showTotal={total => `共 ${total} 条`}
                                defaultPageSize={12}
                                defaultCurrent={1}
                                onChange={pageChange}
                            />
                        </div>
                    </div>
                </div>
            </Drawer>
        </div>
    );
}
