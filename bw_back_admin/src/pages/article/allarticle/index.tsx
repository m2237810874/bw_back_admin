import React, { useEffect, useRef, useState } from 'react'
import style from './allarticle.less'
import { Badge, Tag, Space, Button, Popconfirm, message , Select} from 'antd';
import type { ColumnsType } from 'antd/es/table';
import { useDispatch, useSelector } from 'dva'
import moment from 'moment';
import { history } from 'umi'
import { ProTable } from '@ant-design/pro-components';
import { useRequest } from 'ahooks'
import { get_article, } from '@/api'
import BaseSelect from '@/components/baseSelects'
import { edit_article, del_article } from '../../../api/index'
import { ReloadOutlined } from '@ant-design/icons';
import { my_publics } from '@/api/article'


type Props = {}

interface tags {
  label: string
}
interface DataType {
  key: React.Key;
  title: string;
  status: string;
  label: string,
  views: number,
  tags: Array<tags>,
  likes: number,
  publishAt: string,
  category: any
}


const index = (props: Props) => {
  const dispatch = useDispatch()
  const { runAsync } = useRequest(get_article, {
    manual: true
  })
  const labelList: any = []
  useEffect(() => {
    dispatch({
      type: 'classify/getTag'
    })
  }, [dispatch])

  const { tagList } = useSelector(({ classify }: { classify: any }) => {
    return {
      ...classify
    }
  })
  console.log(tagList);
  tagList.forEach((item: any) => {
    labelList.push({
      title: item.label,
      key: item.id
    })
  })
  console.log(labelList);

  interface ActionType {
    reload: (resetPageIndex?: boolean) => void;
    reloadAndRest: () => void;
    reset: () => void;
    clearSelected?: () => void;
    startEditable: (rowKey: any) => boolean;
    cancelEditable: (rowKey: any) => boolean;
  }
  const confirm = async (record: any) => {
    console.log(record.id);

    const res = await del_article(record.id)
    if (res.statusCode === 200) {
      ref.current.reload()
      message.success('删除成功');
    }
  };

  const ref = useRef<ActionType>();
  function Color() {
    const r = Math.floor(Math.random() * 200)
    const g = Math.floor(Math.random() * 200)
    const b = Math.floor(Math.random() * 200)
    const color = `rgba(${r},${g},${b},${0.8})`
    return color
  }
  const columns: any = [
    {
      title: '标题',

      dataIndex: 'title',
      key: 'title',
      render: (text: any) => <a style={{
        color: '#000'
      }}>{text}</a>,
      fixed: 'left',
      width: '10%'
    },
    {
      title: '状态',
      dataIndex: 'status',
      key: 'status',
      width: '10%',
      valueType: 'select',
      valueEnum: {
        publish: { text: '已发布' },
        draft: { text: '草稿' },
      },
      render: (text: any, record: any) => <span>{record.status === 'publish' ? <Badge status='success'></Badge> : <Badge status='error'></Badge>}{record.status === 'publish' ? '已发布' : '草稿'}</span>,
    },
    {
      title: '分类',
      dataIndex: 'category',
      key: 'category',
      width: '10%',
      valueType: 'select',
      renderFormItem: (item: any, { type, defaultRender, ...rest }: any) => {
        return <Select placeholder={'请选择'}>
          {
            labelList.map((item: any) => {
              return <Select.Option key={item.key}>
                {item.title}
              </Select.Option>
            })
          }
        </Select>
      },
      render: (text: any, record: any, _: any, action: any) => (
        <span>{record.category ? <Tag color={Color()}>{record.category.label}</Tag> : ''}</span>
      ),
    },
    {
      title: '标签',
      key: 'tags',
      dataIndex: 'tags',
      width: '10%',
      hideInSearch: true,
      render: (_: any, { tags }: any) => (
        <>
          {tags.map((tag: any, index: number) => {
            return (
              <Tag key={index} style={{ opacity: '0.5' }} color={Color()}>
                {tag.label}
              </Tag>
            );
          })}
        </>
      ),
    },
    {
      title: '阅读量',
      dataIndex: 'views',
      key: 'views',
      width: '10%',
      hideInSearch: true,
      render: (views: any) => <Tag style={{
        borderRadius: '50%'
      }} color="green">{views}</Tag>,
    },
    {
      title: '喜欢数',
      dataIndex: 'likes',
      key: 'likes',
      width: '10%',
      hideInSearch: true,
      render: (likes: any) => <Tag style={{
        borderRadius: '50%'
      }} color="red">{likes}</Tag>,
    },
    {
      title: '发布时间',
      dataIndex: 'publishAt',
      key: 'publishAt',
      width: '15%',
      hideInSearch: true,
      render: (time: any) => (
        <span style={{ display: 'inline-block', width: '200px' }}>
          {moment(time).format('YYYY-MM-DD hh:mm:ss')}
        </span>
      ),
    },
    {
      title: '操作',
      dataIndex: 'isRecommended',
      key: 'isRecommended',
      fixed: 'right',
      width: '16%',
      hideInSearch: true,
      render: (text: any, record: any) => (
        <Space size="middle" style={{ width: '250px' }}>
          <a onClick={() => history.push('/article/editor/' + record.id, {
            state: {
              record
            }
          })}>编辑</a>
          <a onClick={async () => {
            const res = await edit_article({
              id: record.id,
              isRecommended: !record.isRecommended
            })
            if (res.statusCode === 200) {
              ref.current.reload()
              message.success('操作成功')
            }
          }}>{record.isRecommended ? '撤销首焦' : '首焦推荐'}</a>
          <a>查看访问</a>
          <Popconfirm placement="top" title={'你确认要删除这个文章吗？'} onConfirm={() => confirm(record)} okText="确认" cancelText="取消">
            <a>删除</a>
          </Popconfirm>
        </Space>
      ),
    },
  ];

  const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);

  const onSelectChange = (newSelectedRowKeys: React.Key[]) => {
    console.log('selectedRowKeys changed: ', newSelectedRowKeys);
    setSelectedRowKeys(newSelectedRowKeys);
  };

  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange
  }

  console.log(selectedRowKeys)
  const confirmAll=()=>{
    selectedRowKeys.forEach((item:any)=>{
      del_article(item)
    })
    ref.current.reload();
    message.info('删除成功')
  }

  const All_NoArticle=()=>{
    selectedRowKeys.forEach(async (item:any)=>{
      await edit_article({
        id:item,
        isRecommended:true
      })
      ref.current.reload();
    })
    message.info('操作成功')
  }

  const All_Article=()=>{
    selectedRowKeys.forEach(async (item:any)=>{
      await edit_article({
        id:item,
        isRecommended:false
      })
      ref.current.reload();
    })
    message.info('操作成功')
  }

  const All_Public=()=>{
    selectedRowKeys.forEach(async (item:any)=>{
      await my_publics({
        id:item,
        status:true
      })
      ref.current.reload();
    })
    message.info('操作成功')
  }

  const All_NoPublic=()=>{
    selectedRowKeys.forEach(async (item:any)=>{
      await my_publics({
        id:item,
        status:false
      })
      ref.current.reload();
    })
    message.info('操作成功')
  }


  return (
    <div className={style.allarticle}>
      <header>
        <nav>
          <ol>
            <li>
              <span>
                <a href="/workbench">工作台</a>
              </span>
            </li>&ensp;/&ensp;
            <li>
              <span>
                <a href="/article/allarticle">所有文章</a>
              </span>
            </li>
          </ol>
        </nav>
      </header>
      <main>
        <div className={style.allarticle_main}>
          <ProTable
            actionRef={ref}
            columns={columns}
            sticky
            scroll={{ x: 1500 }}
            rowSelection={
              {
                type: 'checkbox'
              }
            }
            rowKey={'id'}
            toolbar={{
              actions: [
                < Button type="primary" onClick={() => history.push('/article/markdown')} >+新建</Button>
              ],
              settings: [
                {
                  icon: <ReloadOutlined />,
                  tooltip: '刷新',
                },
              ],
            }}
            search={
              {
                searchText: '搜索',
                span: 5,
                optionRender: (searchConfig, formProps, dom) => {
                  console.log(formProps);

                  return dom;
                }
              }
            }
            request={
              async (options: any) => {//初始化的创建表格执行，分页改变执行，点击查询的时候也会执行
                options = {
                  ...options,
                  page: options.current
                }
                delete options.current
                const { data } = await runAsync(options);
                console.log(data);
                return {
                  data: data[0],
                  success: true,
                  total: data[1]
                }
              }
            }
            pagination={{
              showSizeChanger: true
            }}
            tableAlertRender={({ selectedRowKeys, selectedRows, onCleanSelected }) => (
              <div>
                <Button style={{marginRight:'15px'}} onClick={()=>All_Public()}>
                  发布
                </Button>
                <Button style={{marginRight:'15px'}} onClick={()=>All_NoPublic()}>
                  草稿
                </Button>
                <Button style={{marginRight:'15px'}} onClick={()=>All_NoArticle()}>
                  首焦推荐
                </Button>
                <Button style={{marginRight:'15px'}} onClick={()=>All_Article()}>
                  撤销首焦
                </Button>
                <Popconfirm
                  title="确认删除吗"
                  onConfirm={()=>confirmAll()}
                  okText="确认"
                  cancelText="取消"
                >
                  <Button type="primary" danger>
                    删除
                  </Button>
                </Popconfirm>
              </div>
              
            )}
          >  
          </ProTable>
        </div>
      </main>
    </div>
  )
}

export default index