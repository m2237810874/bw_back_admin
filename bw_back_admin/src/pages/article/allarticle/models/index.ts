import {get_article} from '@/api'
const allarticle:any={
    namespace:'allarticle',
    state:{
        labelList:[]
    },
    reducers:{
        get_article(state:any,{payload}:any){
            payload.forEach((item:any)=>{
                if(item.category){
                    allarticle.state.labelList.push(item.category.label)
                }
            })
            return{
                ...state,
                labelList:[...allarticle.state.labelList]
            } 
        },
    },
    effects:{
        *getArticle (_:any,{put,call}:any){
            const data:{data:any[]}=yield call(get_article)
            yield put({
                type:'get_article',
                payload:[...data.data[0]]
            })
        }
    }
}
export default allarticle