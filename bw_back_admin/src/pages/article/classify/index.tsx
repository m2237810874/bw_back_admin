
import React, { useEffect, useState } from 'react'
import style from './classify.less'
import { Input, Button, Tag, Form, Popconfirm,message } from 'antd';
import { useSelector, useDispatch } from 'dva'
import { add_category, del_category, edit_category } from '../../../api/index'
const { CheckableTag } = Tag;
type Props = {}

const index = (props: Props) => {
  const dispatch = useDispatch()
  const [form] = Form.useForm();
  const [flag, setflag] = useState(false)
  const [categoryid, setcategoryid] = useState('')
  useEffect(() => {
    dispatch({
      type: 'classify/getTag'
    })
  }, [dispatch])
  const { tagList } = useSelector(({ classify }: { classify: any }) => {
    return {
      ...classify
    }
  })
  const confirm = async () => {
    let res= await del_category(categoryid)
    if(res.statusCode===200){
      dispatch({
        type: 'classify/getTag'
      })
      setflag(false)
      form.resetFields();
      form.setFieldsValue('')
      message.success('删除分类成功')
    }
  };
  return (
    <div className={style.class}>
      <header>
        <nav>
          <ol>
            <li>
              <span>
                <a href="/workbench">工作台</a>
              </span>
            </li>&ensp;/&ensp;
            <li>
              <span>
                <a href="/article/allarticle">所有文章</a>
              </span>
            </li>&ensp;/&ensp;
            <li>
              <span>
                <a href="/article/classify">分类管理</a>
              </span>
            </li>
          </ol>
        </nav>
      </header>
      <main>
        <div className={style.class_main}>
          <div className={style.class_main_left}>
            <div className={style.class_main_left_top}>
              <p>添加分类</p>
            </div>
            <Form form={form} className={style.class_main_left_bottom} onFinish={async (values) => {
              if (flag) {
                let res=await edit_category({
                  id:categoryid,
                  label:values.label,
                  value:values.value
                })
                if(res.statusCode===200){
                  dispatch({
                    type: 'classify/getTag'
                  })
                  setflag(false)
                  form.resetFields();
                  form.setFieldsValue('')
                  message.success('更新分类成功')
                }
              }
              else {
                let res= await add_category(values)
                if(res.statusCode===201){
                  dispatch({
                    type: 'classify/getTag'
                  })
                  form.resetFields();
                  form.setFieldsValue('')
                  message.success('添加分类成功')
                }
              }
            }}>
              <Form.Item name='label' className={style.class_mian_left_bottom_input}>
                <Input placeholder="输入分类名称" />
              </Form.Item>
              <Form.Item name='value' className={style.class_mian_left_bottom_input}>
                <Input placeholder="输入分类值（请输入英文，作为路由使用）" />
              </Form.Item>
              <div>
                {
                  flag ?
                    <div style={{
                      display: 'flex',
                      justifyContent: 'space-between'
                    }}>
                      <div>
                        <Form.Item style={{ display: 'inline-block' }}>
                          <Button type='primary' htmlType='submit'>
                            更新
                          </Button>
                        </Form.Item>
                        <Button onClick={() => {
                          setflag(false)
                          form.resetFields();
                          form.setFieldsValue('')
                        }}>
                          返回添加
                        </Button>
                      </div>
                      <div>
                        <Popconfirm placement="top" title='确认删除这个分类吗' onConfirm={confirm} okText="确认" cancelText="取消">
                          <Button type="dashed" danger>
                            删除
                          </Button>
                        </Popconfirm>
                      </div>
                    </div>
                    : <Form.Item>
                      <Button type='primary' htmlType="submit">
                        保存
                      </Button>
                    </Form.Item>
                }
              </div>
            </Form>
          </div>
          <div className={style.class_main_right}>
            <div className={style.class_main_right_top}>
              <p>所有分类</p>
            </div>
            <div className={style.class_main_right_bottom}>
              {
                tagList.map((item: any, index: number) => {
                  return <div className={style.class_main_right_bottom_list} key={index} onClick={() => {
                    form.resetFields();
                    form.setFieldsValue(item)
                    setflag(true)
                    setcategoryid(item.id)
                  }}>
                    <CheckableTag style={{ color: '#8e8787', width: '100%', display: 'flex', justifyContent: 'center', alignContent: 'center' }} checked={false}>
                      {item.label}
                    </CheckableTag>
                  </div>
                })
              }
            </div>
          </div>
        </div>
      </main>
    </div>
  )
}

export default index