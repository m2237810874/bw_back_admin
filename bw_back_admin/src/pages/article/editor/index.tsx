import { useEffect, useRef, useState } from 'react';
import style from './editor.less';
import MDEditor, { commands } from '@uiw/react-md-editor';
import content from '@/config/defaultArticleCont';
import _ from 'lodash';
import {
  Upload,
  Button,
  message,
  Popconfirm,
  Menu,
  Dropdown,
  Modal,
  Form,
  Input
} from 'antd';
import Drawer from '@/components/drawer';
import {
  UploadOutlined,
  CloseSquareOutlined,
  EllipsisOutlined,
} from '@ant-design/icons';
import { history } from 'umi';
import { _upload } from '@/api/file';
import { useLocation } from 'react-router-dom';
import { editArticle, deleteItem } from '@/api/markdown';
interface Menun {
  title: string;
  type: string;
  level: number;
  key: number;
}
export default function index() {
  const location: any = useLocation();
  const elist = location.state.state.record;
  console.log(elist);
  const [value, setValue] = useState(() => content);
  const [menuData, setmenuData] = useState<Menun[]>([]);
  const preview = useRef<any>();
  const [title, settitle] = useState(elist.title || '');
  const [open, setOpen] = useState(false);
  const [obj, setobj] = useState({});
  const el:any=document.querySelectorAll('.w-md-editor-preview ')
  const [active,setactive]:any=useState(0)
  const [arr,setarr]:any=useState([])
  const newCreate = (val: any) => {
    setobj(val);
  };
  const add = async () => {
    let res = await editArticle({
      ...obj,
      title: title,
      content: value,
      status: 'publish',
      id: elist.id,
    });
    if (res.statusCode === 200) {
      message.success('修改文章成功');
    }
  };
  const showDrawer = () => {
    setOpen(true);
  };
  const onClose = () => {
    setOpen(false);
  };
  const handleChange = (e: any) => {
    // 写文字
    setValue(e);
  };
  const handleUpload = async (file: any) => {
    //覆盖默认上传行为，实现自定义上传
    console.log(file);
    const formData = new FormData();
    formData.append('file', file.file);
    const res = await _upload(formData);
    message.success('图片上传成功');
  };
  const formatMenuData = () => {
    const previewEl = preview.current.querySelector('.w-md-editor-preview');
    const menuDataEl = Array.from(
      previewEl.querySelectorAll('*'),
    ).filter((item: any) => /^H[1-6]$/.test(item.nodeName));
    setmenuData(
      menuDataEl.map((item: any, i: number) => ({
        title: item.innerText,
        type: item.nodeName,
        level: item.nodeName.slice(1) * 1,
        key: i,
      })),
    );
    setarr(menuDataEl)
  };
  const [isModalOpen, setIsModalOpen] = useState(false);
  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleOk = async () => {
    setIsModalOpen(false);
    const res = await deleteItem(elist.id);
    if (res.statusCode === 200) {
      message.success('删除成功');
      history.goBack();
    }
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };
  const menu = (
    <Menu
      items={[
        {
          key: '1',
          label: <a>查看</a>,
        },
        {
          key: '2',
          label: (
            <a
              onClick={() => {
                if (title) {
                  showDrawer();
                } else {
                  message.warning('请输入文章标题');
                }
              }}
            >
              设置
            </a>
          ),
        },
        {
          key: '3',
          label: <a>保存草稿</a>,
        },
        {
          key: '4',
          label: <a onClick={showModal}>删除</a>,
        },
      ]}
    />
  );
  useEffect(() => {
    setTimeout(() => {
      if (preview.current && value) {
        formatMenuData();
      }
    }, 0);
  }, [preview, value]);
  return (
    <div className={style.editor}>
      <header>
        <div className={style.editor_header}>
          <div className={style.editor_header_left}>
            <div className={style.editor_header_box}>
              <div className={style.editor_header_box_left}>
                <Button>
                <Popconfirm
                  title="确认关闭？如果有内容变更，请先保存"
                  onConfirm={() => history.goBack()}
                  okText="确认"
                  cancelText="取消"
                >
                  <CloseSquareOutlined  width='25' height='25px'/>
                </Popconfirm>
                </Button>
              </div>
              <div className={style.editor_header_box_right}>
                <div className={style.editor_header_box_right_input}>
                      <input placeholder='请输入文章标题' style={{
                        border:'0',
                        borderBottom:'1px solid #d9d9d9',
                        width:'300px',
                      }}
                      value={title}
                      onChange={(e) => {
                        settitle(e.target.value);
                      }}>
                      </input>
                </div>
              </div>
            </div>
          </div>
          <div className={style.editor_header_right}>
            <div className={style.editor_header_right_botton}>
              <Button type="primary" onClick={add}>
                发布
              </Button>
            </div>
            <div className={style.editor_header_right_set}>
                <Dropdown overlay={menu}>
                <EllipsisOutlined />
              </Dropdown>
            </div>
          </div>
        </div>
      </header>
      <MDEditor
        className={style.editor}
        value={value}
        onChange={_.throttle(handleChange, 500)}
        ref={(val: any) => {
          if (val) {
            preview.current = val.container;
          }
        }}
        commands={[
          //自定义toolbar
          {
            name: 'test',
            keyCommand: 'test',
            icon: (
              <Upload
                accept="image/png,image/jpeg,image/gif,image/jpg,image/svg"
                showUploadList={false}
                customRequest={handleUpload}
              >
                <Button type="link" icon={<UploadOutlined />}></Button>
              </Upload>
            ),
          },
          commands.divider,
          commands.bold,
          commands.checkedListCommand,
          commands.code,
          commands.fullscreen,
          commands.italic,
          commands.image,
          commands.strikethrough,
          commands.quote,
        ]}
      >
        <MDEditor.Markdown source={value} className={style.con} />
      </MDEditor>
      <div className={style.menu}>
        <h3>大纲</h3>
        {menuData &&
          menuData.map((item,index) => {
            return (
                <p key={index} className={active===index?`${style.active}`:''} style={{ marginLeft: item.level * 6 }} onClick={()=>{
                    setactive(index)
                    el[0].scrollTop=arr[index].offsetTop
                  }}>
                    {item.title}
                  </p>
            );
          })}
      </div>
      <Drawer
        onClose={onClose}
        open={open}
        newCreate={newCreate}
        elist={elist}
      ></Drawer>
      <Modal open={isModalOpen} onOk={handleOk} onCancel={handleCancel}>
        <h1>确认删除？</h1>
        <p>删除内容后，无法恢复</p>
      </Modal>
    </div>
  );
}
