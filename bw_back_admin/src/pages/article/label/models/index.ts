import {get_label} from '../../../../api/index'
const label={
    namespace:'label',
    state:{
        labelList:[]
    },
    reducers:{
        get_label(state:any,{payload}:any){
            return{
                ...state,
                labelList:[...payload]
            } 
        }
    },
    effects:{
        *getLabel (_:any,{put,call}:any){
            const data:{data:any[]}=yield call(get_label)
            console.log(data);
            
            yield put({
                type:'get_label',
                payload:[...data.data]
            })
        }
    }
}
export default label