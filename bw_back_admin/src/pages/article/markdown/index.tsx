import { useEffect, useRef, useState } from 'react';
import style from './markdown.less';
import MDEditor, { commands } from '@uiw/react-md-editor';
import content from '@/config/defaultArticleCont';
import _ from 'lodash';
import { Upload, Button, message, Input, Menu, Form , Popconfirm, Dropdown} from 'antd';
import Drawer from '@/components/drawer';
import {
  UploadOutlined,
  CloseSquareOutlined,
  EllipsisOutlined,
} from '@ant-design/icons';
import { history } from 'umi';
import { _upload } from '@/api/file';
import { addArticle } from '@/api/markdown';
interface Menun {
  title: string;
  type: string;
  level: number;
  key: number;
}
export default function index() {
  const [value, setValue] = useState(() => content);
  const [menuData, setmenuData] = useState<Menun[]>([]);
  const preview = useRef<any>();
  const [title, settitle] = useState('');
  const [open, setOpen] = useState(false);
  const [obj, setobj] = useState({});
  const [arr,setarr]:any=useState([])
  const [active,setactive]:any=useState(0)
  const el:any=document.querySelectorAll('.w-md-editor-preview ')
  // el[0].onscroll=()=>{
  //   arr.forEach((item:any,index:number)=>{
  //     if(el[0].scrollTop>=item.offsetTop){
  //       setactive(index)
  //     }
  //   })
  // }
  const newCreate = (val: any) => {
    setobj(val);
  };
  const add = async () => {
    let res = await addArticle({
      ...obj,
      title: title,
      content: value,
      status: 'publish',
    });
    if (res.statusCode === 201) {
      message.success('添加文章成功');
    }
  };
  const showDrawer = () => {
    setOpen(true);
  };
  const onClose = () => {
    setOpen(false);
  };
  const handleChange = (e: any) => {
    // 写文字
    setValue(e);
  };
  const handleUpload = async (file: any) => {
    //覆盖默认上传行为，实现自定义上传
    console.log(file);
    const formData = new FormData();
    formData.append('file', file.file);
    const res = await _upload(formData);
    message.success('图片上传成功');
  };
  const formatMenuData = () => {
    const previewEl = preview.current.querySelector('.w-md-editor-preview');
    const menuDataEl = Array.from(
      previewEl.querySelectorAll('*'),
    ).filter((item: any) => /^H[1-6]$/.test(item.nodeName));
    setmenuData(
      menuDataEl.map((item: any, i: number) => ({
        title: item.innerText,
        type: item.nodeName,
        level: item.nodeName.slice(1) * 1,
        key: i,
      })),
    );
    setarr(menuDataEl)
  };

  const menu = (
    <Menu
      items={[
        {
          key: '1',
          label: <a>查看</a>,
          disabled: true,
        },
        {
          key: '2',
          label: (
            <a
              onClick={() => {
                if (title) {
                  showDrawer();
                } else {
                  message.warning('请输入文章标题');
                }
              }}
            >
              设置
            </a>
          ),
        },
        {
          key: '3',
          label: <a>保存草稿</a>,
        },
        {
          key: '4',
          label: <a>删除</a>,
          disabled: true,
        },
      ]}
    />
  );

  useEffect(() => {
    setTimeout(() => {
      if (preview.current && value) {
        formatMenuData();
      }
    }, 0);
  }, [preview, value]);
  return (
    <div className={style.markdown}>
      <header>
        <div className={style.markdown_header}>
          <div className={style.markdown_header_left}>
            <div className={style.markdown_header_box}>
              <div className={style.markdown_header_box_left}>
                <Button>
                <Popconfirm
                  title="确认关闭？如果有内容变更，请先保存"
                  onConfirm={() => history.goBack()}
                  okText="确认"
                  cancelText="取消"
                >
                  <CloseSquareOutlined  width='25' height='25px'/>
                </Popconfirm>
                </Button>
              </div>
              <div className={style.markdown_header_box_right}>
                <div className={style.markdown_header_box_right_input}>
                  <Form>
                    <Form.Item style={{
                      marginBottom:'0'
                    }}>
                      <Input placeholder='请输入文章标题' style={{
                        border:'0',
                        borderBottom:'1px solid #d9d9d9',
                        width:'300px',
                      }}
                      onChange={(e) => {
                        settitle(e.target.value);
                      }}>
                      </Input>
                    </Form.Item>
                  </Form>
                </div>
              </div>
            </div>
          </div>
          <div className={style.markdown_header_right}>
            <div className={style.markdown_header_right_botton}>
              <Button type="primary" onClick={add}>
                发布
              </Button>
            </div>
            <div className={style.markdown_header_right_set}>
                <Dropdown overlay={menu}>
                <EllipsisOutlined />
              </Dropdown>
            </div>
          </div>
        </div>
      </header>
      <MDEditor
        className={style.markdown}
        value={value}
        onChange={_.throttle(handleChange, 500)}
        ref={(val: any) => {
          if (val) {
            preview.current = val.container;
          }
        }}
        commands={[
          //自定义toolbar
          {
            name: 'test',
            keyCommand: 'test',
            icon: (
              <Upload
                accept="image/png,image/jpeg,image/gif,image/jpg,image/svg"
                showUploadList={false}
                customRequest={handleUpload}
              >
                <Button type="link" icon={<UploadOutlined />}></Button>
              </Upload>
            ),
          },
          commands.divider,
          commands.bold,
          commands.checkedListCommand,
          commands.code,
          commands.fullscreen,
          commands.italic,
          commands.image,
          commands.strikethrough,
          commands.quote,
        ]}
      >
        <MDEditor.Markdown source={value} className={style.con} />
      </MDEditor>
      <div className={style.menu}>
        <h3>大纲</h3>
        {menuData &&
          menuData.map((item,index) => {
            return (
              <p key={index} className={active===index?`${style.active}`:''} style={{ marginLeft: item.level * 6 }} onClick={()=>{
                setactive(index)
                el[0].scrollTop=arr[index].offsetTop
              }}>
                {item.title}
              </p>
            );
          })}
      </div>
      <Drawer onClose={onClose} open={open} newCreate={newCreate}></Drawer>
    </div>
  );
}
