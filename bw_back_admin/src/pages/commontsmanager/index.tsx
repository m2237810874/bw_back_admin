import React, { useEffect, useState , useRef} from 'react'
import style from './commontsmanager.less'
import { Form, Input, Select, Button, Table, Badge, Space , Popconfirm ,Popover , message ,Modal } from 'antd';
import {useDispatch,useSelector} from 'dva'
import type { ColumnsType } from 'antd/es/table';
import moment from 'moment';
const { Option } = Select;
import { useRequest } from 'ahooks'
import { get_commont } from '@/api/index'
import { ProTable } from '@ant-design/pro-components';
import type { ProColumns } from '@ant-design/pro-components';
import { commit_adopt , commit_no , del_work_commont } from '@/api/work';
import {ReloadOutlined} from '@ant-design/icons';
import CallBack from '@/components/CallBack/CallBack'





type Props = {}

interface tags{
  label:string
}


const index = (props: Props) => {
  const [status]=useState(['未通过','通过'])
  const ref = useRef<any>();
  const { runAsync }=useRequest(get_commont,{
    manual:true
  })

  const confirm= async(id:string)=>{
    const res=await del_work_commont(id);
    if(res.statusCode===200){
      message.success('评论已删除');
      ref.current.reload();//刷新页面
    }
  }

  const adopt=async (id:string)=>{
    const res=await commit_adopt(id);
    if(res.statusCode===200){
      message.success('评论已通过');
      ref.current.reload();//刷新页面
    }
  }

  const refuse=async (id:string)=>{
    const res=await commit_no(id);
    if(res.statusCode===200){
      message.success('评论已禁止');
      ref.current.reload();
    }
  }

  const columns:any = [
    {
      title: '状态',
      dataIndex: 'pass',
      fixed:'left',
      width:'10%',
      valueType: 'select',
      renderFormItem: (item: any, { type, defaultRender, ...rest }: any) => {
        return <Select placeholder={'请选择'}>
          {
            status.map((item: any,index:number) => {
              return <Select.Option key={index}>
                {item}
              </Select.Option>
            })
          }
        </Select>
      },
      render: (_:any,record:any) => <span>{record.pass?<Badge status='success'></Badge>:<Badge status='error'></Badge>}{record.pass ? '通过' : '未通过'}</span>,
    },
    {
      title: '称呼',
      dataIndex: 'name',
      key: 'name',
      render: (text:any) => <a style={{
        color:'#000'
      }}>{text}</a>,
      width:'10%'
    },
    {
      title: '联系方式',
      dataIndex: 'email',
      key: 'email',
      width:'15%',
      render: (text:any) => <span>{text}</span>,
    },
    {
      title: '原始内容',
      dataIndex: 'content',
      key: 'content',
      width:'10%',
      hideInSearch:true,
      render: (text:any,record:any) => (
        <Popover  content={record.content} title="评论详情-原始内容">
          <a>查看内容</a> 
        </Popover>
      ),
    },
    {
      title: 'html内容',
      dataIndex: 'html',
      key: 'html',
      width:'10%',
      hideInSearch:true,
      render: (text:any,record:any) => 
      <Popover content={record.content}  title="评论详情-html内容">
        <a>查看内容</a>
      </Popover>
    },
    {
      title: '管理文章',
      dataIndex: 'status',
      key: 'status',
      width:'10%',
      hideInSearch:true,
      render: (text:any) => 
      <a>文章</a>
    },
    {
      title: '创建时间',
      dataIndex: 'createAt',
      key: 'createAt',
      width:'15%',
      hideInSearch:true,
      render: (time:any) => (
        <span style={{ display: 'inline-block', width: '200px' }}>
          {moment(time).format('YYYY-MM-DD hh:mm:ss')}
        </span>
      ),
    },
    {
      title: '父级评论',
      dataIndex: 'parentCommentId',
      key: 'parentCommentId',
      width:'10%',
      hideInSearch:true,
      render: (text:any) => <span>{text==='null' ? '无' : ''}</span>,
    },
    {
      title: '操作',
      dataIndex: 'status',
      key: 'status',
      width:'20%',
      fixed:'right',
      hideInSearch:true,
      render: (text:any,record:any) => (
        <Space size="middle" style={{ width: '250px' }}>
            <a onClick={()=>adopt(record.id)}>通过</a>
            <a onClick={()=>refuse(record.id)}>拒绝</a>
            <a onClick={()=>callBack(record)}>回复</a>
          <Popconfirm title="确定删除吗" onConfirm={()=>confirm(record.id)} onOpenChange={() => console.log('open change')}>
            <a>删除</a>
          </Popconfirm>
        </Space>
      ),
    },
  ];

  //回复
  const [isModalOpen, setIsModalOpen]:any = useState(false);
  const [All,setAll]=useState()
  const callBack=(list:any)=>{
    setIsModalOpen(true);
    setAll(list)
  }



  const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);

  const onSelectChange = (newSelectedRowKeys: React.Key[]) => {
    console.log('selectedRowKeys changed: ', newSelectedRowKeys);
    setSelectedRowKeys(newSelectedRowKeys);
  };

  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange
  }

  const adoptAll=()=>{
    selectedRowKeys.forEach((item:any)=>{
      commit_adopt(item)
    })
    ref.current.reload();
  }

  const refuseAll=()=>{
    selectedRowKeys.forEach((item:any)=>{
      commit_no(item)
    })
    ref.current.reload();
  }

  const deleteAll=()=>{
    selectedRowKeys.forEach((item:any)=>{
      del_work_commont(item)
    })
    ref.current.reload();
  }

  const handleOk = () => {
    setIsModalOpen(false);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };


  return (
    <div className={style.commontsmanager}>
      <CallBack isModalOpen={isModalOpen} setIsModalOpen={setIsModalOpen} All={All}></CallBack>
      <header>
        <nav>
          <ol>
            <li>
              <span>
                <a href="/workbench">工作台</a>
              </span>
            </li>&ensp;/&ensp;
            <li>
              <span>
                <a href="/commontsmanager">评论管理</a>
              </span>
            </li>
          </ol>
        </nav>
      </header>
      <main>
      <ProTable
        columns={columns}
        actionRef={ref}
        sticky
        scroll={{x:1500}}
        rowSelection={rowSelection}
        rowKey={'id'}
        search={
          {
            searchText:'查询',
            collapsed:false,
            span:5,
            optionRender:(searchConfig,formProps,dom)=>{
              return dom;
            }
          }
        }
        toolbar={{
          settings:[
            {
              icon: <ReloadOutlined />,
              tooltip: '刷新',
            },
          ],
        }}
        request={
          async (options:any)=>{//初始化的创建表格执行，分页改变执行，点击查询的时候也会执行
            options={
              ...options,
              page:options.current
            }
            delete options.current
            const { data }=await runAsync(options);
            console.log(data);
            
            data[0].forEach((item:any)=>{
              console.log(item.pass);
              
            })
            return{
              data:data[0],
              success:true,
              total:data[1]
            }
          }
        }
        pagination={{
          showSizeChanger: true,
        }}
        tableAlertRender={({ selectedRowKeys, selectedRows, onCleanSelected }) => (
          <div>
            <Button style={{marginLeft:'10px'}} onClick={()=>adoptAll()}>
              通过
            </Button>
            <Button style={{marginLeft:'10px'}} onClick={()=>refuseAll()}>
              拒绝
            </Button>
            <Button style={{marginLeft:'10px'}} onClick={()=>deleteAll()}>
              删除
            </Button>
          </div>
        )}
      >
      </ProTable>
      </main>
    </div>
  )
}

export default index