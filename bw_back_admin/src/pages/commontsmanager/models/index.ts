import {get_commont} from '../../../api/index'
const commontsmanager={
    namespace:'commontsmanager',
    state:{
        commontList:[]
    },
    reducers:{
        get_commont(state:any,{payload}:any){
            return{
                ...state,
                commontList:[...payload]
            } 
        }
    },
    effects:{
        *getcommont ({payload}:any,{put,call}:any){
            const data:{data:any[]}=yield call(()=>get_commont(payload))
            yield put({
                type:'get_commont',
                payload:[...data.data]
            })
        }
    }
}
export default commontsmanager