import React, { useState } from 'react'
import style from './emailmanager.less'
import { Form, Input, Button,Table} from 'antd';
import type { ColumnsType } from 'antd/es/table';
type Props = {}


interface tags{
  label:string
}
interface DataType {
  key: React.Key;
  title: string;
  status:string;
  label:string,
  views:number,
  tags:Array<tags>,
  likes:number,
  publishAt:string,
  category:any,
  role:string
}
const index = (props: Props) => {
  const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);
  const onSelectChange = (newSelectedRowKeys: React.Key[]) => {
    console.log('selectedRowKeys changed: ', selectedRowKeys);
    setSelectedRowKeys(newSelectedRowKeys);
  };
  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  };
  const columns: ColumnsType<DataType> = [
    {
      title: '发件人',
      dataIndex: 'pass',
      key: 'pass',
      width:'20%',
    },
    {
      title: '收件人',
      dataIndex: 'name',
      key: 'name',
      width:'20%'
    },
    {
      title: '主题',
      dataIndex: 'email',
      key: 'email',
      width:'20%',
    },
    {
      title: '发送时间',
      dataIndex: 'content',
      key: 'content',
      width:'20%',
    },
    {
      title: '操作',
      dataIndex: 'status',
      key: 'status',
      width:'20%',
    },
  ];
  return (
    <div className={style.emailmanager}>
      <header>
        <nav>
          <ol>
            <li>
              <span>
                <a href="/workbench">工作台</a>
              </span>
            </li>&ensp;/&ensp;
            <li>
              <span>
                <a href="/article/classify">邮件管理</a>
              </span>
            </li>
          </ol>
        </nav>
      </header>
      <main>
        <div className={style.emailmanager_main}>
          <Form className={style.emailmanager_form}>
              <div className={style.emailmanager_form_head}>
                <div className='emailmanager_form_head_title'>
                  <span>发件人:</span>
                  <Input style={{ width: 200,height:'30px' }} placeholder='请输入发件人'/>
                </div>
                <div className="emailmanager_form_head_state">
                  <span>收件人:</span>
                  <Input style={{ width: 200,height:'30px' }} placeholder='请输入收件人'/>
                </div>
                <div className="emailmanager_form_head_class">
                  <span>主题 :</span>
                  <Input style={{ width: 200,height:'30px' }} placeholder='请输入主题'/>
                </div>
              </div>
              <div className={style.emailmanager_form_bottom}>
                <Button style={{width:'65px',display:'flex',justifyContent:'center',marginRight:'10px'}} type="primary">
                  搜索
                </Button>
                <Button style={{width:'65px',display:'flex',justifyContent:'center'}}>
                  重置
                </Button>
              </div>
          </Form>
          <div className={style.commontsmanager_bottom}>
            <div className={style.commontsmanager_bottom_top}>
              <svg viewBox="64 64 896 896" focusable="false" data-icon="reload" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M909.1 209.3l-56.4 44.1C775.8 155.1 656.2 92 521.9 92 290 92 102.3 279.5 102 511.5 101.7 743.7 289.8 932 521.9 932c181.3 0 335.8-115 394.6-276.1 1.5-4.2-.7-8.9-4.9-10.3l-56.7-19.5a8 8 0 00-10.1 4.8c-1.8 5-3.8 10-5.9 14.9-17.3 41-42.1 77.8-73.7 109.4A344.77 344.77 0 01655.9 829c-42.3 17.9-87.4 27-133.8 27-46.5 0-91.5-9.1-133.8-27A341.5 341.5 0 01279 755.2a342.16 342.16 0 01-73.7-109.4c-17.9-42.4-27-87.4-27-133.9s9.1-91.5 27-133.9c17.3-41 42.1-77.8 73.7-109.4 31.6-31.6 68.4-56.4 109.3-73.8 42.3-17.9 87.4-27 133.8-27 46.5 0 91.5 9.1 133.8 27a341.5 341.5 0 01109.3 73.8c9.9 9.9 19.2 20.4 27.8 31.4l-60.2 47a8 8 0 003 14.1l175.6 43c5 1.2 9.9-2.6 9.9-7.7l.8-180.9c-.1-6.6-7.8-10.3-13-6.2z"></path></svg>
            </div>
            <div className={style.commontsmanager_bottom_main}>
              <Table
                rowSelection={rowSelection}
                columns={columns}
                scroll={{ x: 1500 }}
                sticky
              />
            </div>
          </div>
        </div>
      </main>
    </div>
  )
}

export default index