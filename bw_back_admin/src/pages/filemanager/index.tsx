import { InboxOutlined } from '@ant-design/icons';
import type { UploadProps } from 'antd';
import { message, Upload, Button, Input, Form } from 'antd';
import { Card, Pagination } from 'antd';
import { FC, useEffect, useState } from 'react'
import style from './filemanager.less'
const { Dragger } = Upload;
import { useDispatch, useSelector } from 'umi';
import Brawer from '@/components/Brawer/Brawer'


const gridStyle: React.CSSProperties = {
  width: '25%',
  textAlign: 'center',
};

const index: FC = (props) => {
  const dispatch = useDispatch()
  const [search, setSearch] = useState([])
  console.log(search);
  const [form] = Form.useForm()
  useEffect(() => {
    dispatch({
      type: 'filemanager/get_files',
      payload: {
        page: 1,
        pageSize: 12
      }
    })
  }, [dispatch])

  const { filesdata } = useSelector(({ filemanager }: any) => {
    return {
      ...filemanager
    }
  })


  const pageChange = (page: number, pageSize: number) => {

    dispatch({
      type: 'filemanager/get_files',
      payload: {
        page,
        pageSize
      }
    })
  }


  const [Open, setOpen] = useState(false);
  const [imgData,setImgData]=useState({})
  const onClose = () => {
    setOpen(false);
  };


  const AlertBrawer=(item:any)=>{
    setOpen(true)
    setImgData(item)
  }

  
  return (
    <div className={style.filemanage}>
      <div className={style.filetop}>
        <Dragger {...props}>
          <p className="ant-upload-drag-icon">
            <InboxOutlined />
          </p>
          <p className="ant-upload-text">点击文件将文件拖拽至此处</p>
          <p className="ant-upload-hint">
            文件将上传至oss，如若未配置请先配置
          </p>
        </Dragger>
      </div>

      <div className={style.filefilter}>
        <div className={style.filterinsed}>
          <Form form={form} className={style.posrermanager_main_bottom_form_head} onFinish={(values) => {
            setSearch(filesdata[0].filter((item:any)=>{
              return item.url.includes(values.url)
            }))
            setSearch(filesdata[0].filter((item:any)=>{
              return item.type.includes(values.type)
            }))
            
          }}>
            <Form.Item name='url' >
              <div style={{
                display: 'flex',

              }}>
                <p style={{ display: 'flex', alignItems: 'center', width: '80px' }}>文件名称 :</p>
                <Input style={{ width: 180, height: '30px' }} placeholder='请输入文件名称' />
              </div>
            </Form.Item>
            <Form.Item name='type'>
              <div style={{
                display: 'flex',

              }}>
                <p style={{ display: 'flex', alignItems: 'center', width: '80px' }}>文件类型 :</p>
                <Input style={{ width: 180, height: '30px' }} placeholder='请输入文件名称' />
              </div>
            </Form.Item>
            <div className={style.posrermanager_main_bottom_form_bottom}>
              <Form.Item style={{ display: 'flex' }}>
                <Button style={{ width: '65px', display: 'flex', justifyContent: 'center', marginRight: '10px' }} type="primary" htmlType='submit'>
                  搜索
                </Button>
              </Form.Item>
              <Button style={{ width: '65px', display: 'flex', justifyContent: 'center' }}>
                重置
              </Button>
            </div>
          </Form>
        </div>
      </div>

      {/* 右侧抽屉 */}
      <Brawer onClose={onClose} Open={Open} imgData={imgData}/>



      <div className={style.filecon}>
        <Card>
          {
            filesdata[0] ? filesdata[0].map((item: any, index: number) => {
              return <Card.Grid key={index} className={style.Myantcard}>
                <img src={item.url} alt="" />
                <p>{item.originalname}</p>
                <p>上传于{item.createAt}</p>
              </Card.Grid>
            })
              : ""
          }
        </Card>
        <Pagination
          total={filesdata[1]}
          pageSize={12}
          showSizeChanger
          showQuickJumper
          onChange={pageChange}
          showTotal={total => `总共 ${total} 条数据`}
        />
      </div>
    </div>
  )
}

export default index