import { get_files } from '@/api/filemanage'
const filemanager={
    namespace:"filemanager",
    state:{
        filesdata:[]
    },
    reducers:{
        get_file(state:any,{payload}:any){
            return{
                ...state,
                filesdata:[...payload]
            } 
        }
    },
    effects:{
        *get_files({payload}:any,{put,call}:any){
            const data:{data:any[]}= yield call(()=>get_files(payload))
            yield put({
                type:'get_file',
                payload:[...data.data]
            })
        }
    }
}

export default filemanager