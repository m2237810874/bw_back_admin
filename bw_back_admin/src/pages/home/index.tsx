import { Layout , Menu } from 'antd';
import {FC} from 'react'
import './home.less'
const { Header, Footer, Sider, Content } = Layout;
import { Link } from 'umi'


type Props = {
    children: any;
}

const index = (props: Props) => {
  return (
    <div>
        {props.children}
    </div>
  )
}

export default index