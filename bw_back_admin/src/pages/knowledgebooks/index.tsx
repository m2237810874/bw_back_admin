import { CSSProperties, useEffect, useState } from 'react'
import { Input, Select, Button, Form, Tooltip, Drawer, Switch, Card } from 'antd';
import style from "./index.less"
import { useRequest } from "ahooks"
import { get_knowledge } from "../../api"
import { del_knowledge, edit_knowList, edit_knowStatus, add_knowList } from '@/api/knowledge';
import { EditOutlined, EllipsisOutlined, SettingOutlined, DeleteOutlined, CloudUploadOutlined, InboxOutlined } from '@ant-design/icons';
import { Avatar, Pagination, Popconfirm } from 'antd';
import { useDispatch, useSelector } from 'dva'
const { Option } = Select;
const { Meta } = Card;
import type { UploadProps } from 'antd';
import { message, Upload } from 'antd';
const { Dragger } = Upload;
type Props = {}

const gridStyle: React.CSSProperties = {
  width: '23%',
  padding: '0',
  textAlign: 'center',
};
const propsd: UploadProps = {
  name: 'file',
  multiple: true,
  action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
  onChange(info) {
    const { status } = info.file;
    if (status !== 'uploading') {
      console.log(info.file, info.fileList);
    }
    if (status === 'done') {
      message.success(`${info.file.name} file uploaded successfully.`);
    } else if (status === 'error') {
      message.error(`${info.file.name} file upload failed.`);
    }
  },
  onDrop(e) {
    console.log('Dropped files', e.dataTransfer.files);
  },
};
const addpropsd: UploadProps = {
  name: 'file',
  multiple: true,
  action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
  onChange(info) {
    const { status } = info.file;
    if (status !== 'uploading') {
      console.log(info.file, info.fileList);
    }
    if (status === 'done') {
      message.success(`${info.file.name} file uploaded successfully.`);
    } else if (status === 'error') {
      message.error(`${info.file.name} file upload failed.`);
    }
  },
  onDrop(e) {
    console.log('Dropped files', e.dataTransfer.files);
  },
};

const fileprops: UploadProps = {
  name: 'file',
  action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
  headers: {
    authorization: 'authorization-text',
  },
  onChange(info) {
    if (info.file.status !== 'uploading') {
      console.log(info.file, info.fileList);
    }
    if (info.file.status === 'done') {
      message.success(`${info.file.name} file uploaded successfully`);
    } else if (info.file.status === 'error') {
      message.error(`${info.file.name} file upload failed.`);
    }
  },
};
const index = (props: Props) => {
  const dispatch = useDispatch()
  const [form] = Form.useForm();
  const [addform] = Form.useForm();
  const [open, setOpen] = useState(false);
  const [addopen, setaddOpen] = useState(false);
  const [fileopen, setfileOpen] = useState(false);
  const [drawerList, setdrawerList]:any = useState([])
  useEffect(() => {
    dispatch({
      type: 'knowledgebooks/getKnowledge'
    })
    dispatch({
      type: 'knowledgebooks/knowList',
      payload: {
        page: 1,
        pageSize: 12
      }
    })
  }, [dispatch])
  const { knowledgeList, knowList } = useSelector(({ knowledgebooks }: { knowledgebooks: any }) => {
    return {
      ...knowledgebooks
    }
  })
  console.log(knowList[0]);
  const pageChange = (page: number, pageSize: number) => {
    dispatch({
      type: 'knowledgebooks/knowList',
      payload: {
        page,
        pageSize
      }
    })
  }
  const confirm = async (id: string) => {
    let res = await del_knowledge(id)
    dispatch({
      type: "knowledgebooks/getKnowledge"
    })
  }
  const [current, setcurrent] = useState(1)
  const [pageSize] = useState(6)
  const knowledgelist = knowledgeList.slice((current - 1) * pageSize, current * pageSize)
  const [drawerform]: any = Form.useForm();
  const handleChange = (value: string) => {
    console.log(`selected ${value}`);
  };
  const [searchList, setsearchList] = useState([])
  console.log(knowledgeList);
  const [drawerImg, setdrawerImg] = useState('')
  const [adddrawerImg, setadddrawerImg] = useState('')
  const showDrawer = (item: any) => {
    setOpen(true);
    drawerform.resetFields();
    drawerform.setFieldsValue(item)
    setdrawerList(item)
    setdrawerImg(item.cover)
  };
  console.log(drawerList);

  const onClose = () => {
    setOpen(false);
  };
  const addonClose = () => {
    setaddOpen(false);
  };


  const fileshowDrawer = () => {
    setfileOpen(true);
  };
  const fileonClose = () => {
    setfileOpen(false);
  };
  const [imglis, setImglis] = useState('')
  return (
    <div className={style.knowledge}>
      <header>
        <nav>
          <ol>
            <li>
              <span>
                <a href="/workbench">工作台</a>
              </span>
            </li>&ensp;/&ensp;
            <li>
              <span>
                <a href="/knowledgebooks">知识小册</a>
              </span>
            </li>
          </ol>
        </nav>
      </header>
      <main>
        <Form form={form} onFinish={(values) => {
         if(values.title){
          setsearchList( knowledgeList.filter((item:any)=>{
            return item.title.includes(values.title)
          }))
         }
         if(values.status){
          setsearchList( knowledgeList.filter((item:any)=>{
            return item.status.includes(values.status==='已发布'?'publish':'draft')
          }))
         }
        }}>
          <div className={style.know_search}>
            <div className={style.know_top}>
              <div style={{ display: 'flex', alignItems: 'center', margin: '30px 70px 0 30px' }}>
                <span>名称:</span>
                <Form.Item name='title' style={{ marginLeft: '24px', marginBottom: '0', display: 'flex', alignItems: 'flex-end', }}>
                  <Input style={{ width: 200 }} placeholder="请输入页面名称" />
                </Form.Item>
              </div>
              <p>
                <span>状态:</span>
                <Form.Item name='status' style={{ marginLeft: '24px', marginBottom: '0', display: 'flex', alignItems: 'flex-end', }}>
                  <Select style={{ width: 200 }} onChange={handleChange}>
                    <Option value="已发布">已发布</Option>
                    <Option value="草稿">草稿</Option>
                  </Select>
                </Form.Item>
              </p>
            </div>
            <div className={style.know_bot}>
              <p>
                <Form.Item style={{ display: 'inline-block', marginBottom: '0' }}>
                  <Button type="primary" htmlType="submit" style={{ margin: 10 }}>搜索</Button>
                </Form.Item>
                <Button onClick={()=>{
                  setsearchList([])
                }}>重置</Button></p>
            </div>
          </div>
          <div className={style.know_list}>
            <p><Button type="primary" style={{ margin: 10 }} onClick={() => {
              setaddOpen(true)
            }}>+新建</Button></p>
            <div className={style.know_listitem}>
              {
                searchList.length ?
                  searchList.map((item: any, index: number) => {
                    return <Card
                      key={index}
                      style={{ width: '25%', maxWidth: '25%', marginLeft: '8px', marginRight: '8px' }}
                      cover={
                        <img
                          alt="example"
                          src={item.cover}
                        />
                      }
                      actions={[
                        <EditOutlined key="edit" />,
                        <Tooltip placement="top" title={item.status === 'publish' ? '设为草稿' : '发布线上'}>
                          <CloudUploadOutlined />,
                        </Tooltip>,
                        <SettingOutlined key="setting" onClick={() => showDrawer(item)} />,
                        <EllipsisOutlined key="ellipsis" />,
                      ]}
                    >
                      <Meta
                        title={item.title}
                        description={item.summary}
                      />
                    </Card>

                  }) : knowledgelist.map((item: any, index: number) => {
                    return <Card
                      key={index}
                      style={{ width: '25%', maxWidth: '25%', marginLeft: '8px', marginRight: '8px' }}
                      cover={
                        <img
                          alt="example"
                          src={item.cover}
                        />
                      }
                      actions={[
                        <EditOutlined key="edit" />,
                        <Tooltip placement="top" title={item.status === 'publish' ? '设为草稿' : '发布线上'}>
                          <CloudUploadOutlined onClick={async () => {
                            const res = await edit_knowStatus({
                              id: item.id,
                              status: item.status === 'publish' ? 'draft' : 'publish'
                            })
                            if (res.statusCode === 200) {
                              dispatch({
                                type: "knowledgebooks/getKnowledge"
                              })
                            }
                          }} />,
                        </Tooltip>,
                        <SettingOutlined key="setting" onClick={() => showDrawer(item)} />,
                        <Popconfirm
                          title="确认删除吗?"
                          onConfirm={() => confirm(item.id)}
                          okText="Yes"
                          cancelText="No"
                        >
                          <DeleteOutlined />
                        </Popconfirm>

                      ]}
                    >
                      <Meta
                        title={item.title}
                        description={item.summary}
                      />
                    </Card>

                  })
              }
            </div>
            <div className={style.know_page}> <Pagination defaultCurrent={1} total={knowledgeList.length} pageSize={pageSize} current={current} onChange={(page) => {
              setcurrent(page)
            }} /></div>
          </div>
        </Form>
      </main>
      <Drawer title="新建知识库" width='600px' placement="right" onClose={addonClose} open={addopen}>
        <div className={style.drawer}>
          <Form form={addform} onFinish={async (values) => {
            const res = await add_knowList({
              ...values,
              id: drawerList.id,
              cover: adddrawerImg ? adddrawerImg : ''
            })
            if (res.statusCode === 201) {
              dispatch({
                type: 'knowledgebooks/getKnowledge'
              })
            }
            setaddOpen(false)
            addform.resetFields();
            addform.setFieldsValue('')
            setadddrawerImg('')
          }}>
            <div className={style.drawer_header}>
              <div className="drawer_header_name">
                <span>名称</span>
                <Form.Item name='title' style={{ display: 'inline-block', marginBottom: '0', width: '90%' }}>
                  <Input style={{ width: '100%' }} type="text" />
                </Form.Item>
              </div>
              <div className="drawer_header_desc">
                <span>描述</span>
                <Form.Item name='summary' style={{ display: 'inline-block', marginBottom: '0', width: '90%' }}>
                  <Input style={{ width: '100%', height: '70px' }} type="text" />
                </Form.Item>
              </div>
              <div className="drawer_header_commend">
                <span>评论</span>
                <Form.Item name='isCommentable' valuePropName="checked" style={{ display: 'inline-block', marginBottom: '0' }}>
                  <Switch>
                    <svg className="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="3476" width="200" height="200"><path d="M514.048 128q79.872 0 149.504 30.208t121.856 82.432 82.432 122.368 30.208 150.016q0 78.848-30.208 148.48t-82.432 121.856-121.856 82.432-149.504 30.208-149.504-30.208-121.856-82.432-82.432-121.856-30.208-148.48q0-79.872 30.208-150.016t82.432-122.368 121.856-82.432 149.504-30.208z" p-id="3477" data-spm-anchor-id="a313x.7781069.0.i3" class="" fill="#ffffff"></path></svg>
                  </Switch>
                </Form.Item>
              </div>
              <div className="drawer_header_img">
                <span>封面</span>
                <div style={{ display: 'inline-block', marginBottom: '0', width: '90%' }}>
                  <Form.Item name='cover'>
                    <Dragger {...addpropsd}>
                      {
                        adddrawerImg ?
                          <img width='100%' height='200px' src={adddrawerImg} alt="" />
                          : <div>
                            <p className="ant-upload-drag-icon">
                              <InboxOutlined />
                            </p>
                            <p className="ant-upload-text">Click or drag file to this area to upload</p>
                            <p className="ant-upload-hint">
                              Support for a single or bulk upload. Strictly prohibit from uploading company data or other
                              band files
                            </p>
                          </div>
                      }
                    </Dragger>
                    <Input value={adddrawerImg ? adddrawerImg : ''}></Input>
                  </Form.Item>
                  <Button style={{ marginRight: '15px' }} onClick={fileshowDrawer}>选择文件</Button>
                  {/* {
                    drawerList.cover || imglis ?
                      <Button danger onClick={() => {
                        setdrawerImg('')
                        setImglis('')
                      }}>移除</Button>
                      : ''
                  } */}
                </div>
              </div>
            </div>
            <div className={style.drawer_footer}>
              <Button style={{ marginRight: '15px' }}>取消</Button>
              <Button type="primary" htmlType='submit'>添加</Button>
            </div>
          </Form>
        </div>
      </Drawer>
      <Drawer title="更新知识库" width='600px' placement="right" onClose={onClose} open={open}>
        <div className={style.drawer}>
          <Form form={drawerform} onFinish={async (values) => {
            const res = await edit_knowList({
              ...values,
              id: drawerList.id,
              cover: imglis ? imglis : drawerImg
            })
            if (res.statusCode === 200) {
              dispatch({
                type: 'knowledgebooks/getKnowledge'
              })
            }
            setOpen(false)
            setImglis('')
          }}>
            <div className={style.drawer_header}>
              <div className="drawer_header_name">
                <span>名称</span>
                <Form.Item name='title' style={{ display: 'inline-block', marginBottom: '0', width: '90%' }}>
                  <Input style={{ width: '100%' }} type="text" />
                </Form.Item>
              </div>
              <div className="drawer_header_desc">
                <span>描述</span>
                <Form.Item name='summary' style={{ display: 'inline-block', marginBottom: '0', width: '90%' }}>
                  <Input style={{ width: '100%', height: '70px' }} type="text" />
                </Form.Item>
              </div>
              <div className="drawer_header_commend">
                <span>评论</span>
                <Form.Item name='isCommentable' valuePropName="checked" style={{ display: 'inline-block', marginBottom: '0' }}>
                  <Switch defaultChecked>
                    <svg className="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="3476" width="200" height="200"><path d="M514.048 128q79.872 0 149.504 30.208t121.856 82.432 82.432 122.368 30.208 150.016q0 78.848-30.208 148.48t-82.432 121.856-121.856 82.432-149.504 30.208-149.504-30.208-121.856-82.432-82.432-121.856-30.208-148.48q0-79.872 30.208-150.016t82.432-122.368 121.856-82.432 149.504-30.208z" p-id="3477" data-spm-anchor-id="a313x.7781069.0.i3" class="" fill="#ffffff"></path></svg>
                  </Switch>
                </Form.Item>
              </div>
              <div className="drawer_header_img">
                <span>封面</span>
                <div style={{ display: 'inline-block', marginBottom: '0', width: '90%' }}>
                  <Form.Item name='cover'>
                    <Dragger {...propsd}>
                      {
                        drawerImg || imglis ?
                          // <Form.Item name='cover' valuePropName='src'>
                          <img width='100%' height='200px' src={imglis ? imglis : drawerImg} alt="" />
                          // </Form.Item>
                          : <div>
                            <p className="ant-upload-drag-icon">
                              <InboxOutlined />
                            </p>
                            <p className="ant-upload-text">Click or drag file to this area to upload</p>
                            <p className="ant-upload-hint">
                              Support for a single or bulk upload. Strictly prohibit from uploading company data or other
                              band files
                            </p>
                          </div>
                      }
                    </Dragger>
                    <Input value={imglis ? imglis : drawerImg}></Input>
                  </Form.Item>
                  <Button style={{ marginRight: '15px' }} onClick={fileshowDrawer}>选择文件</Button>
                  {
                    drawerList.cover || imglis ?
                      <Button danger onClick={() => {
                        setdrawerImg('')
                        setImglis('')
                      }}>移除</Button>
                      : ''
                  }
                </div>
              </div>
            </div>
            <div className={style.drawer_footer}>
              <Button style={{ marginRight: '15px' }} onClick={() => {
                setOpen(false)
              }}>取消</Button>
              <Button type="primary" htmlType='submit'>更新</Button>
            </div>
          </Form>
        </div>
      </Drawer>
      <Drawer width='780px' title="文件选择" placement="right" onClose={fileonClose} open={fileopen}>
        <div className={style.file}>
          <Form>
            <div className={style.file_top}>
              <div className="file_name">
                <span>文件名</span>
                <Form.Item style={{ display: 'inline-block', marginBottom: '0' }}>
                  <Input placeholder='请输入文件名称'></Input>
                </Form.Item>
              </div>
              <div className="file_class">
                <span>文件类</span>
                <Form.Item style={{ display: 'inline-block', marginBottom: '0' }}>
                  <Input placeholder='请输入文件类型'></Input>
                </Form.Item>
              </div>
            </div>
            <div className={style.file_bottom}>
              <Button type="primary" style={{ marginRight: '15px' }}>搜索</Button>
              <Button>重置</Button>
            </div>
          </Form>
          <div className={style.file_list}>
            <div className={style.file_list_top}>
              <Upload {...fileprops}>
                <Button>上传文件</Button>
              </Upload>
            </div>
            <div className='file_list_bottom' style={{ marginTop: '20px' }}>
              <Card>
                {
                  knowList[0] && knowList[0].map((item: any, index: number) => {
                    return <Card.Grid key={index} style={gridStyle} onClick={() => {
                      setImglis(item.url)
                      setfileOpen(false)
                      setadddrawerImg(item.url)
                    }}>
                      <img width='100%' height='100px' src={item.url} alt="" style={{ objectFit: 'cover' }} />
                      <p style={{
                        whiteSpace: 'nowrap',
                        textOverflow: 'ellipsis',
                        overflow: 'hidden',
                        display: 'block',
                        padding: '20px 10px 20px 10px'
                      }}>{item.originalname}</p>
                    </Card.Grid>
                  })
                }
              </Card>
            </div>
            <div className={style.pagesition}>
              <Pagination
                total={knowList[1]}
                showTotal={total => `共 ${total} 条`}
                defaultPageSize={12}
                defaultCurrent={1}
                onChange={pageChange}
              />
            </div>
          </div>
        </div>
      </Drawer>

    </div>
  )
}

export default index