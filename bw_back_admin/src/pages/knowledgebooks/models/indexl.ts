import { get_knowList } from '@/api/knowledge'
import { includes } from 'lodash'
import {get_knowledge} from '../../../api/index'
const knowledgebooks={
    namespace:'knowledgebooks',
    state:{
        knowledgeList:[],
        knowList:[]
    },
    reducers:{
        get_knowledge(state:any,{payload}:any){
            return{
                ...state,
                knowledgeList:[...payload]
            } 
        },
        get_knowList(state:any,{payload}:any){
            return{
                ...state,
                knowList:[...payload]
            }
        }
    },
    effects:{
        *getKnowledge (_:any,{put,call}:any){
            const data:{data:any[]}=yield call(get_knowledge)
            yield put({
                type:'get_knowledge',
                payload:[...data.data[0]]
            })
        },
        *knowList({payload}:any,{put,call}:any){
            const data:{data:any[]}=yield call(()=>get_knowList(payload))
            console.log(data);
            
            yield put({
                type:'get_knowList',
                payload:[...data.data]
            })
        }
    }
}
export default knowledgebooks