import { _login } from '@/api/login'
const loginModel={
    namespace:"loginModel",
    state:{
        user:JSON.parse(localStorage.getItem('user') as any)===''?{}:JSON.parse(localStorage.getItem('user') as any)
    },
    reducers:{
        get_user(state:any,{payload}:any){
            return{
                ...state,
                user:payload.data
            } 
        }
    },
    effects:{
        
    }
}

export default loginModel