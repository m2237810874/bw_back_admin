import { EllipsisOutlined, PlusOutlined ,ReloadOutlined} from '@ant-design/icons';
import type { ProColumns } from '@ant-design/pro-components';
import { ProTable } from '@ant-design/pro-components';
import { Popconfirm , Space , Button,message} from 'antd';
import { useRef , FC, useEffect , useState} from 'react';
import { useRequest } from 'ahooks';
import { get_look_all } from '@/api/lookall'
import style from './lookall.less'
import { delete_look } from '@/api/lookall'



type  GithubIssueItem ={
  address:string,
  browser:string,
  count:number,
  createAt:string,
  device:string,
  id: string;
  ip: string;
  os:string,
  updateAt:string,
  url:string,
  userAgent:string
}


const index:FC = () => {
  const { runAsync }=useRequest(get_look_all,{
    manual:true
  })
  const ref = useRef<any>();
  const confirm = (id:string) => {
    delete_look(id)
    ref.current.reload();
    message.success('操作成功')
  };
  
  const cancel = (e: React.MouseEvent<HTMLElement>) => {
    console.log(e);
    
  };

  const columns: ProColumns<GithubIssueItem>[] = [
    {
      title:'URL',
      dataIndex: 'url',
      fixed:'left'
    },
    {
      title:'IP',
      dataIndex: 'ip',
    },
    {
      title:'浏览器',
      dataIndex: 'browser',
    },
    {
      title:'内核',
      dataIndex: 'engine',
    },
    {
      title:'	操作系统',
      dataIndex: 'os',
    },
    {
      title:'设备',
      dataIndex: 'device',
    },
    {
      title:'	地址',
      dataIndex: 'address',
    },
    {
      title:'访问量',
      render:(text, record, _, action)=>[<span>{record.count}</span>]
    },
    {
      title:'访问时间',
      hideInSearch:true,
      render:(text, record, _, action)=>[<span>{record.updateAt}</span>]
    },
    {
      title:'操作',
      dataIndex: 'work',
      valueType:'indexBorder',
      hideInSearch:true,
      fixed:'right',
      render: (text, record, _, action) => [
        <Popconfirm
          title="确认删除吗"
          onConfirm={()=>confirm(record.id)}
          okText="确认"
          cancelText="取消"
        >
          <a key="2">删除</a>
        </Popconfirm>
      ],
    },
  ]


  const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);

  const onSelectChange = (newSelectedRowKeys: React.Key[]) => {
    console.log('selectedRowKeys changed: ', newSelectedRowKeys);
    setSelectedRowKeys(newSelectedRowKeys);
  };

  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange
  }
  
  const confirmAll=()=>{
    selectedRowKeys.forEach((item:any)=>{
      delete_look(item)
    })
    ref.current.reload();
    message.success('操作成功')
  }


  return (
    <div>
      <header className={style.header}>
        <nav>
          <ol>
            <li>
              <span>
                <a href="/workbench">工作台</a>
              </span>
            </li>&ensp;/&ensp;
            <li>
              <span>
                <a href="/lookall">访问统计</a>
              </span>
            </li>
          </ol>
        </nav>
      </header>
      <div  className={style.out}>
      <ProTable
        columns={columns} 
        actionRef={ref}
        sticky
        scroll={{x:1500}}
        rowSelection={rowSelection}
        rowKey={'id'}
        search={
          {
            searchText:'查询',
            collapsed:false,
            span:5,
            optionRender:(searchConfig,formProps,dom)=>{
              return dom;
            }
          }
        }
        toolbar={{
          settings:[
            {
              icon: <ReloadOutlined />,
              tooltip: '刷新',
            },
          ],
        }}
        request={
          async (options:any)=>{//初始化的创建表格执行，分页改变执行，点击查询的时候也会执行
            options={
              ...options,
              page:options.current
            }
            delete options.current
            const { data }=await runAsync(options);
            return{
              data:data[0],
              success:true,
              total:data[1]
            }
          }
        }
        pagination={{
          showSizeChanger: true,
        }}
        tableAlertRender={({ selectedRowKeys, selectedRows, onCleanSelected }) => (
          <Popconfirm
          title="确认删除吗"
          onConfirm={()=>confirmAll()}
          okText="确认"
          cancelText="取消"
        >
          <Button type="primary" danger>
            删除选中
          </Button>
        </Popconfirm>
        )}
      >
        
          
      </ProTable>
      </div>
    </div>
  )
}

export default index