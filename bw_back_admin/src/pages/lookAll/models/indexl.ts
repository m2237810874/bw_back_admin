import { get_look_all } from '@/api/lookall'
const lookAll={
    namespace:"lookAll",
    state:{
        allLook:[]
    },
    reducers:{
        get_user(state:any,{payload}:any){
            return{
                ...state,
                allLook:[...payload]
            } 
        }
    },
    effects:{
        *get_look(_:any,{put,call}:any){
            const data:{data:any[]}= yield call(get_look_all)
            
            yield put({
                type:'get_user',
                payload:[...data.data[0]]
            })
        }
    }
}

export default lookAll