import React, { useEffect, useState } from 'react';
import { Avatar, Input, Button } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import { updateUser } from '@/api/my';
import style from '../less/index.less'
function Setting() {
  const user = JSON.parse(localStorage.getItem('user') as string);
  const [name, setName] = useState(user.name);
  const [email, setEmail] = useState(user.email);

  return (
    <div>
      <div className={style.header_img}>
        {user && user.avatar ? (
          <img src={user.avatar} alt="" style={{ width:100,height:100,padding: 20,borderRadius:'50%'}} />
        ) : (
          <Avatar size="large" icon={<UserOutlined />} />
        )}
      </div>
      <div>
        <p>
          <span>{'用户名'}</span>
          <Input value={name} onChange={(e) => setName(e.target.value)} />
        </p>
        <p>
          <span>{'邮箱'}</span>
          <Input value={email} onChange={(e) => setEmail(e.target.value)} />
        </p>
      </div>
      <Button
        onClick={() => {
          user.email = email;
          user.name = name;
          updateUser({ ...user }).then((res) => {
            localStorage.setItem('user', JSON.stringify(res.data));
          });
        }}
      >
        保存
      </Button>
    </div>
  );
}

export default Setting;
