import React, { useState } from 'react';
import { Input, Button, message } from 'antd';
import { updatePsw } from '@/api/my';
import { history } from 'umi';
function UpdatePsw() {
  const [oldPassword, setOldPsw] = useState('');
  const [newPassword, setNewPsw] = useState('');
  const [surePassword, setSurePsw] = useState('');
  const update = () => {
    if (!oldPassword || !newPassword || !surePassword) {
      message.warning('输入框不能为空');
      return;
    } else if (newPassword !== surePassword) {
      message.error('两次密码输入不一致');
      return;
    } else if (newPassword.length < 9) {
      message.error('密码长度太短');
      return;
    } else {
      updatePsw({
        oldPassword,
        newPassword,
        ...JSON.parse(localStorage.getItem('user') as string)
      }).then((res) => {
        if(res.statusCode >=200 && res.statusCode < 300){
          message.success('密码修改成功，请重新登录');
          history.push('/login')
        }else{
          message.error(res.msg);
        }
      });
    }
  };
  return (
    <div>
      <p>
        <span>{'原密码'}</span>
        <Input.Password onChange={(e) => setOldPsw(e.target.value)} />
      </p>
      <p>
        <span>{'新密码'}</span>
        <Input.Password onChange={(e) => setNewPsw(e.target.value)} />
      </p>
      <p>
        <span>{'确认密码'}</span>
        <Input.Password onChange={(e) => setSurePsw(e.target.value)} />
      </p>
      <Button onClick={() => {update()}}>更新</Button>
    </div>
  );
}

export default UpdatePsw;
