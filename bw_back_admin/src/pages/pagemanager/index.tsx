import { useRequest } from 'ahooks'
import { useRef } from 'react'
import { Input, Select, Button, Badge, Tag, Space, Popconfirm, message } from 'antd';
import style from "./index.less"
import { ProTable, TableDropdown, ProColumns, ActionType } from '@ant-design/pro-components';
import BaseSelects from "../../components/baseSelects"
import { ReloadOutlined } from '@ant-design/icons';
import { get_pagemanager, edit_pagemanager, del_pagemanager } from "../../api/index"
import { history } from 'umi';
const { Option } = Select;
type Props = {}

const index = (props: Props) => {
  const handleChange = (value: string) => {
    console.log(`selected ${value}`);
  };
  const actionRef = useRef<any>()

  const { runAsync } = useRequest(get_pagemanager, {
    manual: true
  })
  const confirm =async (record:any) => {
    const res= await del_pagemanager(record.id)
    if(res.statusCode===200){
      actionRef.current.reload()
      message.success('删除成功');
    }
  };
  
  const columns: any = [
    {
      title: '名称',
      dataIndex: 'name',
    },
    {
      title: '路径',
      dataIndex: 'path',
    },
    {
      title: '顺序',
      dataIndex: 'order',
      hideInSearch: true,
    },
    {
      title: '阅读量',
      dataIndex: 'views',
      hideInSearch: true,
      render: (views: any) => <Tag style={{
        borderRadius: '50%'
      }} color="green">{views}</Tag>,
    },
    {
      title: '状态',
      dataIndex: 'status',
      valueType: 'select',
      valueEnum: {
        draft: {
          text: '草稿',
          status: 'Error',
        },
        publish: {
          text: '已发布',
          status: 'Success',
        },
      },
      render: (text: any, record: any) => <span>{record.status === 'publish' ? <Badge status='success'></Badge> : <Badge status='error'></Badge>}{record.status === 'publish' ? '已发布' : '草稿'}</span>,
    },
    {
      title: '发布时间',
      dataIndex: 'publishAt',
      hideInSearch: true,
    },
    {
      title: '操作',
      dataIndex: 'status',
      hideInSearch: true,
      render: (text: any, record: any) => {
        return <Space className={style.make}>
          <a onClick={()=>history.push('/editpage/'+record.id,{
            state:{
              record
            }
          })}>编辑</a>
          <a onClick={() => {
            edit_pagemanager({
              id: record.id,
              status: record.status === 'publish' ? 'draft' : 'publish'
            })
            actionRef.current.reload()
            message.success('操作成功')
          }}>{record.status === 'publish' ? '下线' : '发布'}</a>
          <a>查看访问</a>
          <Popconfirm placement="top" title='确认删除这个文章？' onConfirm={()=>confirm(record)} okText="确认" cancelText="取消">
            <a>删除</a>
          </Popconfirm>
        </Space>
      }
    }

  ]
  return (
    <div className={style.pagemanager}>
      <header>
        <nav>
          <ol>
            <li>
              <span>
                <a href="/workbench">工作台</a>
              </span>
            </li>&ensp;/&ensp;
            <li>
              <span>
                <a href="/pagemanager">页面管理</a>
              </span>
            </li>
          </ol>
        </nav>
      </header>
      <div className={style.page_body}>
        {/* <p><Button type="primary" style={{margin:10}}>+新建</Button></p> */}
        <div className={style.page_table}>
          <ProTable
            rowSelection={
              {
                type: "checkbox"
              }
            }
            actionRef={actionRef}
            columns={columns}
            rowKey={"id"}
            pagination={{
              showSizeChanger: true,
            }}
            search={{
              searchText: "查询",
              span: 5,
              optionRender: (searchConfig, formProps, dom) => {
                return dom
              }
            }}
            toolbar={{
              actions: [
                < Button type="primary" onClick={()=>history.push(`/addpage`)} >+新建</Button>
              ],
              settings: [
                {
                  icon: <ReloadOutlined />,
                  tooltip: '刷新',
                },
              ],
            }}
            request={
              async (options: any) => {
                options = {
                  ...options,
                  page: options.current,
                }
                delete options.current
                const { data } = await runAsync(options)
                console.log(data);

                return {
                  data: data[0],
                  success: true,
                  total: data[1]
                }
              }
            }
          >

          </ProTable>
        </div>
      </div>
    </div>
  )
}

export default index