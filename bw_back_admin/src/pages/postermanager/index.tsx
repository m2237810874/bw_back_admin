import React from 'react'
import style from './postermanager.less'
import {Form, Input, Button} from 'antd'
type Props = {}

const index = (props: Props) => {
  return (
    <div className={style.postermanager}>
      <header>
        <nav>
          <ol>
            <li>
              <span>
                <a href="/workbench">工作台</a>
              </span>
            </li>&ensp;/&ensp;
            <li>
              <span>
                <a href="/posrermanager">海报管理</a>
              </span>
            </li>
          </ol>
        </nav>
      </header>
      <main>
        <div className={style.posrermanager_main}>
          <div className={style.posrermanager_main_top}>
            <input type="file" />
            <div className={style.posrermanager_main_top_box}>
              <p><svg viewBox="0 0 1024 1024" fontSize='48px' color='#29a2ff' focusable="false" data-icon="inbox" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M885.2 446.3l-.2-.8-112.2-285.1c-5-16.1-19.9-27.2-36.8-27.2H281.2c-17 0-32.1 11.3-36.9 27.6L139.4 443l-.3.7-.2.8c-1.3 4.9-1.7 9.9-1 14.8-.1 1.6-.2 3.2-.2 4.8V830a60.9 60.9 0 0060.8 60.8h627.2c33.5 0 60.8-27.3 60.9-60.8V464.1c0-1.3 0-2.6-.1-3.7.4-4.9 0-9.6-1.3-14.1zm-295.8-43l-.3 15.7c-.8 44.9-31.8 75.1-77.1 75.1-22.1 0-41.1-7.1-54.8-20.6S436 441.2 435.6 419l-.3-15.7H229.5L309 210h399.2l81.7 193.3H589.4zm-375 76.8h157.3c24.3 57.1 76 90.8 140.4 90.8 33.7 0 65-9.4 90.3-27.2 22.2-15.6 39.5-37.4 50.7-63.6h156.5V814H214.4V480.1z"></path></svg></p>
              <p>点击选择文件或将文件拖拽到此处</p>
              <p>文件将上传到OSS，如未配置请先配置</p>
            </div>
          </div>
          <div className={style.posrermanager_main_bottom}>
            <Form className={style.posrermanager_main_bottom_form}>
              <div className={style.posrermanager_main_bottom_form_head}>
                <div className={style.posrermanager_main_bottom_form_title}>
                  <p>文件名称 :</p>
                  <Input style={{ width: 180,height:'30px' }} placeholder='请输入文件名称'/>
                </div>
              </div>
              <div className={style.posrermanager_main_bottom_form_bottom}>
                <Button style={{width:'65px',display:'flex',justifyContent:'center',marginRight:'10px'}} type="primary">
                  搜索
                </Button>
                <Button style={{width:'65px',display:'flex',justifyContent:'center'}}>
                  重置
                </Button>
              </div>
            </Form>
            <div className={style.posrermanager_main_bottom_List}>
              <div className="posrermanager_main_bottom_List_image">
                <svg className="ant-empty-img-simple" width="64" height="41" viewBox="0 0 64 41" xmlns="http://www.w3.org/2000/svg"><g transform="translate(0 1)" fill="none" fill-rule="evenodd"><ellipse class="ant-empty-img-simple-ellipse" cx="32" cy="33" rx="32" ry="7"></ellipse><g className="ant-empty-img-simple-g" fill-rule="nonzero"><path d="M55 12.76L44.854 1.258C44.367.474 43.656 0 42.907 0H21.093c-.749 0-1.46.474-1.947 1.257L9 12.761V22h46v-9.24z"></path><path d="M41.613 15.931c0-1.605.994-2.93 2.227-2.931H55v18.137C55 33.26 53.68 35 52.05 35h-40.1C10.32 35 9 33.259 9 31.137V13h11.16c1.233 0 2.227 1.323 2.227 2.928v.022c0 1.605 1.005 2.901 2.237 2.901h14.752c1.232 0 2.237-1.308 2.237-2.913v-.007z" className="ant-empty-img-simple-path"></path></g></g></svg>
              </div>
              <div className={style.posrermanager_main_bottom_List_title}>
                暂无数据
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  )
}

export default index