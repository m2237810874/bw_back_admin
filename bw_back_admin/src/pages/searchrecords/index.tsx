import { Button , Input , Table , Popconfirm ,message} from 'antd';
import React, { useEffect ,useState , useRef} from 'react'
import { useDispatch, useSelector , } from 'umi';
import type { ProColumns } from '@ant-design/pro-components';
import style from './search.less'
import { ProTable } from '@ant-design/pro-components';
import {useRequest} from 'ahooks'
import { get_searchlist } from '@/api/search'
import { del_searchList } from '@/api/search'
import {ReloadOutlined} from '@ant-design/icons';


type Props = {}


const index = (props: Props) => {
  const { runAsync }=useRequest(get_searchlist,{
    manual:true
  })


  const ref = useRef<any>();

  const confirm = (id:string) =>{
    del_searchList(id)
    ref.current.reload();
    message.success('操作成功')
  }


  const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);

  const onSelectChange = (newSelectedRowKeys: React.Key[]) => {
    console.log('selectedRowKeys changed: ', newSelectedRowKeys);
    setSelectedRowKeys(newSelectedRowKeys);
  };

  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange
  }

  const confirmAll=()=>{
    selectedRowKeys.forEach((item:any)=>{
      del_searchList(item)
    })
    ref.current.reload();
    message.success('操作成功')
  }

  const columns:any = [
    {
      title: '搜索词',
      dataIndex: 'keyword',
      key: 'keyword',
    },
    {
      title: '搜索量',
      dataIndex: 'count',
      key: 'count',
    },
    {
      title: '搜索时间',
      dataIndex: 'updateAt',
      key: 'updateAt',
    },
    {
      title: '操作',
      hideInSearch:true,
      fixed:'right',
      render:(text:any, record:any, _:any, action:any)=>(
        <Popconfirm title="确定要删除吗" onConfirm={()=>confirm(record.id)} onOpenChange={() => console.log('open change')}>
          <a>删除</a> 
        </Popconfirm>
      )
    },
  ];
  return (
    <div className={style.search}>
      <header className={style.header}>
        <nav>
          <ol>
            <li>
              <span>
                <a href="/workbench">工作台</a>
              </span>
            </li>&ensp;/&ensp;
            <li>
              <span>
                <a href="/lookall">访问统计</a>
              </span>
            </li>
          </ol>
        </nav>
      </header>
      <div className={style.searmain}>
        <div className="searchcon">
            <ProTable
            columns={columns}
            rowSelection={rowSelection}
            actionRef={ref}
            sticky
            scroll={{x:1500}}
            rowKey={'id'}
            search={
              {
                searchText:'查询',
                span:5,
                optionRender:(searchConfig,formProps,dom)=>{
                  console.log(searchConfig,formProps,dom);
                  return dom;
                }
              }
            }
            request={
              async (options:any)=>{//初始化的创建表格执行，分页改变执行，点击查询的时候也会执行
                options={
                  ...options,
                  page:options.current
                }
                delete options.current
                const { data }=await runAsync(options);
                return{
                  data:data[0],
                  success:true,
                  total:data[1]
                }
              }
            }
            toolbar={{
              settings:[
                {
                  icon: <ReloadOutlined />,
                  tooltip: '刷新',
                },
              ],
            }}
            pagination={{
              showSizeChanger: true,
            }}
            tableAlertRender={({ selectedRowKeys, selectedRows, onCleanSelected }) => (
              <Popconfirm
              title="确认删除吗"
              onConfirm={()=>confirmAll()}
              okText="确认"
              cancelText="取消"
            >
              <Button type="primary" danger>
                删除选中
              </Button>
            </Popconfirm>
            )}
          >
          </ProTable>
        </div>
      </div>
    </div>
  )
}

export default index