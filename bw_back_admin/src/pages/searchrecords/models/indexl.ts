import { get_searchlist } from '@/api/search'
const search={
    namespace:"search",
    state:{
        searchlist:[]
    },
    reducers:{
        get_search(state:any,{payload}:any){
            return{
                ...state,
                searchlist:[...payload]
            } 
        }
    },
    effects:{
        *get_searchs({payload}:any,{put,call}:any){
            const data:{data:any[]}= yield call(()=>get_searchlist(payload))
            yield put({
                type:'get_search',
                payload:[...data.data]
            })
        }
    }
}

export default search