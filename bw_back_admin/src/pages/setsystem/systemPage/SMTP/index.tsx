import React, { useState, useEffect } from 'react';
import { Input, Button , Form } from 'antd';
import styles from "./index.less"
import { useRequest } from "ahooks"



const { TextArea } = Input;

type Props = {}
// SMTP
const Statistics = (props: Props) => {
    const [form] = Form.useForm();

    return (
        <Form
        form={form}    
        >
            <Form.Item>
                <p>SMTP 地址</p>
                <Input placeholder="Basic usage" />
            </Form.Item>
            <Form.Item>
                <p>SMTP 端口（强制使用 SSL 连接）</p>
                <Input placeholder="Basic usage" />
            </Form.Item>
            <Form.Item>
                <p>SMTP 用户</p>
                <Input placeholder="Basic usage" />
            </Form.Item>
            <Form.Item>
                <p>SMTP 密码</p>
                <Input placeholder="Basic usage" />
            </Form.Item>
            <Form.Item>
                <p>发件人</p>
                <Input placeholder="Basic usage" />
            </Form.Item>
            <Form.Item>
                <Button type="primary">保存</Button>
                <Button>测试</Button>
            </Form.Item>
        </Form>

    )
}

export default Statistics