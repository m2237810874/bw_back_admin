import React, { useState, useEffect } from 'react';
import { Input, Button } from 'antd';
import styles from "./index.less"
const { TextArea } = Input;
import { get_system } from '@/api/system'
import { useRequest } from 'ahooks'





type Props = {}
// 系统设置
const Settings = (props: Props) => {

    const {data}=useRequest(get_system,{

    })

    return (
        <div>
            <div>
                <p>系统地址</p>
                <Input placeholder="Basic usage" value='www.com' />
            </div>
            <div>
                <p>后台地址</p>
                <Input placeholder="Basic usage" value='creationadmin.shbwyz.com'/>
            </div>
            <div>
                <p>系统标题</p>
                <Input placeholder="Basic usage" value='ikun'/>
            </div>
            <div>
                <p>Logo</p>
                <Input placeholder="Basic usage" value='https://bwcreation.oss-cn-beijing.aliyuncs.com/2022-09-05/2.gif'/>
            </div>
            <div>
                <p>Favicon</p>
                <Input placeholder="Basic usage" value='https://bwcreation.oss-cn-beijing.aliyuncs.com/2022-09-05/2.gif'/>
            </div>
            <div>
                <p>页脚信息</p>
                <TextArea rows={4} value='index'/>
            </div>
            <Button type="primary">保存</Button>
        </div>
    )
}

export default Settings