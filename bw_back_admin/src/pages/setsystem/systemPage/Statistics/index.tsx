import React, { useState, useEffect } from 'react';
import { Input, Button } from 'antd';
import styles from "./index.less"
const { TextArea } = Input;

type Props = {}
//数据统计
const Statistics = (props: Props) => {
    return (
        <div>
            <div>
                <p>百度统计</p>
                <Input placeholder="Basic usage" value='2f616121a4be61774c494d106870f30e'/>
            </div>
            <div>
                <p>谷歌分析</p>
                <Input placeholder="Basic usage" value='G-10SK76KWMSS'/>
            </div>

            <Button type="primary">保存</Button>
        </div>
    )
}

export default Statistics