import React, { useState, useEffect } from 'react';
import styles from "./index.less"
import { Tabs } from "antd";
import { useRequest } from "ahooks";
const { TabPane } = Tabs;
//国际化
const initialPanes = [
    {
        title: "en", content: "123",
        key: "1"
    },
    {
        title: "zh", content: "222",
        key: "2"
    },
];
export default function Internationalization() {
    var newTabIndex = 0;
    const [activeKey, setactiveKey] = useState(initialPanes[0].key)
    const [panes, setpanes] = useState(initialPanes)
    function onChange(activeKey: any) {
        setactiveKey(activeKey)

    };
    function add() {
        const activeKey = `newTab${newTabIndex++}`;
        const newPanes = [...panes];
        newPanes.push({ title: 'New Tab', content: 'Content of new Tab', key: activeKey });
        setactiveKey(activeKey)
        setpanes(newPanes)
    };
    return (
        <div>
            <Tabs
                type='editable-card'
                onChange={onChange}
                activeKey={activeKey}
            >
                {panes.map(pane => (
                    <TabPane tab={pane.title} key={pane.key}>
                        {pane.content}
                    </TabPane>
                ))
                }
            </Tabs>
        </div>
    )


}

