import React, { useState,useEffect } from 'react';
import styles from "./index.less"
import { Input ,Button  } from 'antd';
const { TextArea } = Input;

type Props = {}
//SEO
const SEOPage = (props: Props) => {
    return (
        <div>
        <div>
            <p>关键词</p>
            <Input placeholder="Basic usage" value='JavaScript,TypeScript,Vue.js,微信小程序,React.js,正则表达式,WebGL,Webpack,Docker,MVVM,nginx,java",   "seoDesc": "“小楼又清风”是 fantasticit（https://github.com/fantasticit）的个人小站。本站的文章包括：前端、后端等方面的内容，也包括一些个人读书笔记。'/>
        </div>
        <div className={styles.information}>
            <p>描述信息</p>
            <TextArea rows={4} value='“小楼又清风”是 fantasticit（https://github.com/fantasticit）的个'/>
        </div>
        <Button type="primary" className={styles.btn}>保存</Button>
    </div>
    )
}

export default SEOPage