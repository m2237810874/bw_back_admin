export interface tableParams {
    page: number;
    pageSize: number;
  }
  //登录
  export interface loginData {
    name: string;
    password: string;
  }
  
  //用户
  export interface userData {
    id: string;
    avatar: string;
    email: string;
    role: string;
    status: boolean;
    token: string;
    name: string;
    updateAt: string;
  }
  export interface updatePswData extends userData{
    oldPassword: string;
    newPassword: string;
  }
  export interface responseType {
    statusCode: number;
    msg: string | null;
    success: boolean;
    data: any;
  }
  