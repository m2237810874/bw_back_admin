import React, { useEffect, useState , useRef } from 'react'
import style from './users.less'
import { Form, Input, Select, Button , Badge, Space ,message} from 'antd';
import moment from 'moment';
import { ProTable } from '@ant-design/pro-components';
import { useRequest } from 'ahooks';
import { get_user } from '@/api/index'
import { updata_user,updata_user_role } from '@/api/user'
import {ReloadOutlined} from '@ant-design/icons';

type Props = {}


const index = (props: Props) => {
  const { runAsync }=useRequest(get_user,{
      manual:true
  })
  const ref = useRef<any>();
  const columns: any = [
    {
      title: '账户',
      dataIndex: 'name',
      key: 'name',
      render: (text:any) => <a style={{
        color:'#000'
      }}>{text}</a>,
      width:'10%'
    },
    {
      title: '邮箱',
      dataIndex: 'email',
      key: 'email',
      width:'20%',
      render: (text:any) => <span>{text}</span>,
    },
    {
      title: '角色',
      dataIndex: 'role',
      key: 'role',
      width:'15%',
      valueType: 'select',
      valueEnum: {
        admin: {
          text: '管理员',
        },
        visitor: {
          text: '访客',
        },
      },
      render: (text:any,record:any) => (
        <span>{record.role?record.role==='visitor'?'访客':'管理员':'访客'}</span>
      ),
    },
    {
      title: '状态',
      dataIndex: 'status',
      key: 'status',
      width:'15%',
      valueType: 'select',
      valueEnum: {
        active: {
          text: '可用',
        },
        locked: {
          text: '锁定',
        },
      },
      render: (text:any,record:any) => <span>{record.status==='active'?<Badge status='success'></Badge>:<Badge status='error'></Badge>}{record.status === 'active' ? '可用' : '锁定'}</span>,
    },
    {
      title: '注册时间',
      dataIndex: 'createAt',
      key: 'createAt',
      width:'20%',
      hideInSearch:true,
      render: (time:number) => (
        <span style={{ display: 'inline-block', width: '200px' }}>
          {moment(time).format('YYYY-MM-DD hh:mm:ss')}
        </span>
      ),
    },
    {
      title: '操作',
      dataIndex: 'status',
      key: 'status',
      width:'20%',
      hideInSearch:true,
      render: (text:any,record:any) => (
        <Space size="middle" style={{ width: '250px' }}>
          <a onClick={async ()=>{
            const res = await updata_user({
              id:record.id,
              status:record.status === 'active' ? 'locked' : 'active'
            })
            if(res.statusCode===201){
              ref.current.reload();
            }
            message.success('操作成功')
          }}>{record.status === 'active' ? '禁用' : '启用'}</a>
          <a onClick={ async ()=>{
            const res = await updata_user_role({
              id:record.id,
              role:record.role==='admin'?'visitor':'admin'
            })
            if(res.statusCode===201){
              ref.current.reload();
            }
            message.success('操作成功')
          }}>{record.role==='admin'?'解除授权':'授权'}</a>
        </Space>
      ),
    },
  ];

  const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);

  const onSelectChange = (newSelectedRowKeys: React.Key[]) => {
    console.log('selectedRowKeys changed: ', newSelectedRowKeys);
    setSelectedRowKeys(newSelectedRowKeys);
  };

  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange
  }
  
  const openAll=()=>{
    selectedRowKeys.forEach((item:any)=>{
      updata_user({
        id:item,
        status:'active'
      })
    })
    ref.current.reload();
  }

  const closeAll=()=>{
    selectedRowKeys.forEach((item:any)=>{
      updata_user({
        id:item,
        status:'locked'
      })
    })
    ref.current.reload();
  }

  const Noauthorization=()=>{
    selectedRowKeys.forEach((item:any)=>{
      updata_user_role({
        id:item,
        role:'admin'
      })
    })
    ref.current.reload();
  }

  const authorization=()=>{
    selectedRowKeys.forEach((item:any)=>{
      updata_user_role({
        id:item,
        role:'visitor'
      })
    })
    ref.current.reload();
  }




  return (
    <div className={style.user}>
      <header>
        <nav>
          <ol>
            <li>
              <span>
                <a href="/workbench">工作台</a>
              </span>
            </li>&ensp;/&ensp;
            <li>
              <span>
                <a href="/users">用户管理</a>
              </span>
            </li>
          </ol>
        </nav>
      </header>
      <main>
        <div className={style.user_main}>
        <ProTable
        columns={columns}
        rowSelection={rowSelection}
        actionRef={ref}
        sticky
        scroll={{x:1500}}
        rowKey={'id'}
        search={
          {
            searchText:'查询',
            collapsed:false,
            span:5,
            optionRender:(searchConfig,formProps,dom)=>{
              return dom;
            }
          }
        }
        request={
          async (options:any)=>{//初始化的创建表格执行，分页改变执行，点击查询的时候也会执行
            options={
              ...options,
              page:options.current
            }
            delete options.current
            const { data }=await runAsync(options);
            console.log(data);
            
            return{
              data:data[0],
              success:true,
              total:data[1]
            }
          }
        }
        pagination={{
         showSizeChanger:true
        }}
        toolbar={{
          settings:[
            {
              icon: <ReloadOutlined />,
              tooltip: '刷新',
            },
          ],
        }}
        tableAlertRender={({ selectedRowKeys, selectedRows, onCleanSelected }) => (
          <div>
            <Button style={{marginLeft:'10px'}} onClick={()=>openAll()}>
              启用
            </Button>
            <Button style={{marginLeft:'10px'}} onClick={()=>closeAll()}>
              禁用
            </Button>
            <Button style={{marginLeft:'10px'}} onClick={()=>authorization()}>
              解除授权
            </Button>
            <Button style={{marginLeft:'10px'}} onClick={()=>Noauthorization()}>
              授权
            </Button>
          </div>
        )}
      >
      </ProTable>
        </div>
      </main>
    </div>
  )
}

export default index