import {get_user} from '../../../api/index'
const user={
    namespace:'user',
    state:{
        userList:[]
    },
    reducers:{
        get_user(state:any,{payload}:any){
            return{
                ...state,
                userList:[...payload]
            } 
        }
    },
    effects:{
        *getuser (_:any,{put,call}:any){
            const data:{data:any[]}=yield call(get_user)
            yield put({
                type:'get_user',
                payload:[...data.data[0]]
            })
        }
    }
}
export default user