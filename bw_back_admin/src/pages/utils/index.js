//context传值
//context上下文环境
//上下文:全局变量的上下文  全局都能访问得到
//运行一段代码的时候 所要知道的所有变量的集合
//跨组件通信


import { createContext } from 'react'

const {Provider,Consumer}=createContext()

export default{
    Provider,//向该上下文中提供变量
    Consumer//接受
}