import {FC,CSSProperties,useEffect,useState , useRef} from 'react'
import { useDispatch,history, useSelector } from 'umi';
import style from "./index.less"
import BaseChart from "../../components/baseChart"
import { useRequest } from "ahooks"
import { getChartData,_getArticalList } from "../../api"
import { Card , Button , Popconfirm , Popover , message } from "antd"
import { del_work_commont , commit_adopt , commit_no} from '@/api/work'
import CallBack from '@/components/CallBack/CallBack'
import { get_set } from '@/api/index'



type Props={
  children:any
}


const gridStyle:CSSProperties={
  width: '33%',
  textAlign: 'center',
}

const Index = (props:Props)=> {
  const dispatch=useDispatch()
  const res=useRequest(get_set)
  window.localStorage.setItem('syemeater',JSON.stringify(res.data))
  const {loading,data}:any=useRequest(getChartData,{
    pollingInterval:10000
  });
  const {loading:articleLoading,data:articleData}=useRequest(_getArticalList)
  useEffect(()=>{
    dispatch({
      type:'workModel/get_files',
    })
  },[dispatch])
  const {commentdata}=useSelector(({workModel}:any)=>{
    return{
      ...workModel
    }
  })
  const { user } =useSelector(({loginModel}:any)=>{
    return{
      ...loginModel
    }
  })
  const confirm=async (id:string)=>{
    let res=await del_work_commont(id)
    if(res.statusCode===200){
      message.info('评论已删除')
      dispatch({
        type:'workModel/get_files',
      })
    }
  }

  const adopt=async (id:string)=>{
    const res=await commit_adopt(id)
    if(res.statusCode===200){
      message.info('评论已通过')
      dispatch({
        type:'workModel/get_files',
      })
    }
  }

  const refuse=async(id:string)=>{
    const res=await commit_no(id)
    if(res.statusCode===200){
      message.info('评论已禁止')
      dispatch({
        type:'workModel/get_files',
      })
    }
  }


  // 回复
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [All,setAll]=useState()
  const callBack=(list:any)=>{
    setIsModalOpen(true);
    setAll(list)
  }


  return (
    <div className={style.work}>
      <div className={style.work_header}>
        <p>工作台</p>
        <h1>您好,{user.name}</h1>
        <p>您的角色：管理员</p>7
      </div>
      <div className={style.work_echarts}>
         <p>面板导航</p>
         <div className={style.echarts_main}>
          <h4>每周用户访问指标</h4>
             <BaseChart 
               type="line"
               charData={{
                title:{
                  text:"销售统计图",
                  subtext:"周统计图"
                },
                legend:"plain",
               }}
               series={
                [
                  {
                    type:"line",
                    name:"访问量",
                    lineStyle:{
                      color:'#000'
                    },
                    data:data?.data[0]
                  },
                  {
                    type:"bar",
                    name:"成交量",
                    itemStyle: {
                      color: 'skyblue'
                    },
                    data:data?.data[1]
                  }
                ]
               }
               xData={["Mon","Tue","Wed","Thu","Fir"]}
               loading={loading}
               
               width={1300}
               height={400}
             />
         </div>
      </div>
      <div className={style.work_nav}>
        <p>快速导航</p>
        <ul>
          <li onClick={()=>{
            history.replace('/article/allarticle')
          }}>文章管理</li>
          <li onClick={()=>{
            history.replace('/commontsmanager')
          }}>评论管理</li>
          <li onClick={()=>{
            history.replace('/filemanager')
          }}>文件管理</li>
          <li onClick={()=>{
            history.replace('/users')
          }}>用户管理</li>
          <li onClick={()=>{
            history.replace('/lookAll')
          }}>访问管理</li>
          <li onClick={()=>{
            history.replace('/setsystem')
          }}>系统设置</li>
        </ul>
      </div>
      <div className={style.work_list}>
          <p>最新文章</p>
        <div className={style.work_listitem}>
        {
            articleData?.data[0].map((item:any,index:number)=>{
              return  <Card
                        key={index}
                        size='small'
                        style={gridStyle}
                          loading={articleLoading}
                        >
                          <img src={item.cover} alt="" />
                          <p>{item.title}</p>
                      </Card>

            })
          }
        
        </div>
      </div>

      {/* 回复 */}
      <CallBack isModalOpen={isModalOpen} setIsModalOpen={setIsModalOpen} All={All}></CallBack>


      <div className={style.new_comment}>
          <p className={style.comment_title}>最新评论</p>
          <div className={style.comment_list}>
            {
              commentdata[0]?.map((item:any,index:number)=>{
                return <div key={index}>
                        <p className={style.make}>{item.name}在<Button>文章</Button>评论 <Popover content={item.content} title="评论详情-原始内容"><Button>查看内容</Button></Popover> {item.pass?"通过":"未通过"}</p>
                        <p className={style.make}>
                          <Button onClick={()=>adopt(item.id)}>通过</Button>
                          <Button onClick={()=>refuse(item.id)}>拒绝</Button>
                          <Button onClick={()=>callBack(item)}>回复</Button>
                          <Popconfirm
                            title="确认删除吗"
                            onConfirm={()=>confirm(item.id)}
                            okText="确认"
                            cancelText="取消" >
                              删除
                          </Popconfirm>
                        </p>
                      </div>
              })
            }
          </div>
      </div>
    </div>
  )
}

export default Index