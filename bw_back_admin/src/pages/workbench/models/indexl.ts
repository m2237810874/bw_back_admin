import {get_newcomment} from "@/api/index"

const workModel={
    namespace:"workModel",
    state:{
        commentdata:[]
    },
    reducers:{
        get_pull(state:any,{payload}:any){
            return {
                ...state,
                commentdata:[...payload]
            }
        }
    },
    effects:{
        *get_files({payload}:any,{put,call}:any){
            const data:{data:any[]}= yield call(get_newcomment)
            yield put({
                type:'get_pull',
                payload:[...data.data]
            })
        }
    }
}

export default workModel